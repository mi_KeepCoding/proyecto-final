absl-py==0.14.1
adam==0.0.0.dev0
alabaster @ file:///home/ktietz/src/ci/alabaster_1611921544520/work
alembic @ file:///home/conda/feedstock_root/build_artifacts/alembic_1632067939798/work
anaconda-client @ file:///tmp/build/80754af9/anaconda-client_1624473988214/work
anaconda-navigator==2.0.4
anaconda-project @ file:///tmp/build/80754af9/anaconda-project_1626085644852/work
ansi2html==1.6.0
anyio @ file:///tmp/build/80754af9/anyio_1617783275907/work/dist
anyjson @ file:///home/conda/feedstock_root/build_artifacts/anyjson_1620792011962/work
apache-airflow @ file:///tmp/build/80754af9/airflow-split_1605877887939/work
apispec==1.3.3
appdirs==1.4.4
argcomplete @ file:///home/conda/feedstock_root/build_artifacts/argcomplete_1619128689661/work
argh==0.26.2
argon2-cffi @ file:///tmp/build/80754af9/argon2-cffi_1613037097816/work
arrow @ file:///home/conda/feedstock_root/build_artifacts/arrow_1624555980104/work
asn1crypto @ file:///tmp/build/80754af9/asn1crypto_1596577642040/work
astroid @ file:///tmp/build/80754af9/astroid_1628063142195/work
astropy @ file:///tmp/build/80754af9/astropy_1629829212064/work
astunparse==1.6.3
async-generator @ file:///home/ktietz/src/ci/async_generator_1611927993394/work
atomicwrites==1.4.0
attrs==19.3.0
Automat==20.2.0
autopep8 @ file:///tmp/build/80754af9/autopep8_1615918855173/work
Babel @ file:///tmp/build/80754af9/babel_1620871417480/work
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
backports.entry-points-selectable==1.1.0
backports.functools-lru-cache @ file:///tmp/build/80754af9/backports.functools_lru_cache_1618170165463/work
backports.shutil-get-terminal-size @ file:///tmp/build/80754af9/backports.shutil_get_terminal_size_1608222128777/work
backports.tempfile @ file:///home/linux1/recipes/ci/backports.tempfile_1610991236607/work
backports.weakref==1.0.post1
backports.zoneinfo @ file:///home/conda/feedstock_root/build_artifacts/backports.zoneinfo_1617499121463/work
Bashutils==0.0.4
bcrypt @ file:///home/conda/feedstock_root/build_artifacts/bcrypt_1610521160329/work
beautifulsoup4 @ file:///home/linux1/recipes/ci/beautifulsoup4_1610988766420/work
bitarray @ file:///tmp/build/80754af9/bitarray_1629132843052/work
bkcharts==0.2
black==19.10b0
bleach @ file:///tmp/build/80754af9/bleach_1628110601003/work
blis==0.7.5
bokeh @ file:///tmp/build/80754af9/bokeh_1625766910825/work
boto==2.49.0
Bottleneck==1.3.2
Brotli==1.0.9
brotlipy==0.7.0
cached-property @ file:///home/conda/feedstock_root/build_artifacts/cached_property_1615209429212/work
cachetools==4.2.4
catalogue==2.0.6
cattrs==1.0.0
certifi==2021.10.8
cffi @ file:///home/conda/feedstock_root/build_artifacts/cffi_1606236101993/work
chardet @ file:///tmp/build/80754af9/chardet_1607706746162/work
chart-studio==1.1.0
clang==5.0
click==7.1.2
clifactory==0.1.1
cloudpickle @ file:///tmp/build/80754af9/cloudpickle_1598884132938/work
cloudscraper==1.2.58
clyent==1.2.2
colorama @ file:///tmp/build/80754af9/colorama_1607707115595/work
colorlog==6.4.1
colour==0.1.5
conda==4.10.3
conda-build==3.21.4
conda-content-trust @ file:///tmp/build/80754af9/conda-content-trust_1617045594566/work
conda-pack @ file:///tmp/build/80754af9/conda-pack_1611163042455/work
conda-package-handling @ file:///tmp/build/80754af9/conda-package-handling_1618262148928/work
conda-repo-cli @ file:///tmp/build/80754af9/conda-repo-cli_1620168426516/work
conda-token @ file:///tmp/build/80754af9/conda-token_1620076980546/work
conda-verify==3.4.2
conf==0.4.1
configparser @ file:///home/conda/feedstock_root/build_artifacts/configparser_1614550305463/work
constantly==15.1.0
contextlib2==0.6.0.post1
crayons==0.4.0
croniter @ file:///home/conda/feedstock_root/build_artifacts/croniter_1604369816489/work/dist
cryptography @ file:///tmp/build/80754af9/cryptography_1616769286105/work
cssselect==1.1.0
cycler==0.10.0
cymem==2.0.6
Cython @ file:///tmp/build/80754af9/cython_1626256955500/work
cytoolz==0.11.0
dash==2.3.0
dash-auth==1.4.1
dash-bootstrap-components==1.0.3
dash-core-components==2.0.0
dash-html-components==2.0.0
dash-iconify==0.1.0
dash-renderer==1.9.1
dash-table==5.0.0
dask @ file:///tmp/build/80754af9/dask-core_1629132726831/work
databricks==0.2
dataclasses @ file:///home/conda/feedstock_root/build_artifacts/dataclasses_1628958434797/work
DBUtils==2.0.2
decorator==4.4.2
defusedxml @ file:///tmp/build/80754af9/defusedxml_1615228127516/work
delta==0.4.2
deltalake==0.5.3
diff-match-patch @ file:///tmp/build/80754af9/diff-match-patch_1594828741838/work
dill @ file:///home/conda/feedstock_root/build_artifacts/dill_1623610058511/work
distlib==0.3.3
distributed @ file:///tmp/build/80754af9/distributed_1629141715288/work
docopt==0.6.2
docutils==0.17.1
entrypoints==0.3
et-xmlfile==1.1.0
etcd-distro==3.5.1
ete3==3.1.2
fastcache==1.1.0
filelock==3.3.2
findspark==1.4.2
flake8 @ file:///tmp/build/80754af9/flake8_1615834841867/work
Flask @ file:///home/ktietz/src/ci/flask_1611932660458/work
Flask-Admin==1.5.4
Flask-AppBuilder==2.2.4
Flask-Babel==1.0.0
Flask-Caching @ file:///home/conda/feedstock_root/build_artifacts/flask-caching_1628873906391/work
Flask-Compress==1.10.1
Flask-JWT-Extended @ file:///home/conda/feedstock_root/build_artifacts/flask-jwt-extended_1588273568824/work
Flask-Login==0.4.1
Flask-OpenID @ file:///home/conda/feedstock_root/build_artifacts/flask-openid_1602336311621/work
Flask-SeaSurf==0.3.1
Flask-SQLAlchemy @ file:///home/conda/feedstock_root/build_artifacts/flask-sqlalchemy_1616102249273/work
flask-swagger==0.2.13
Flask-WTF==0.14.3
flatbuffers==1.12
fonttools==4.25.0
fsspec @ file:///tmp/build/80754af9/fsspec_1626383727127/work
funcsigs==1.0.2
furl @ file:///home/conda/feedstock_root/build_artifacts/furl_1622383325663/work
future==0.18.2
gast==0.4.0
gensim==4.0.0
gevent @ file:///tmp/build/80754af9/gevent_1628273677693/work
glob2 @ file:///home/linux1/recipes/ci/glob2_1610991677669/work
gmpy2==2.0.8
google-auth==1.35.0
google-auth-oauthlib==0.4.6
google-pasta==0.2.0
grammar==1.6
graphviz @ file:///home/conda/feedstock_root/build_artifacts/python-graphviz_1609966316231/work
greenlet @ file:///tmp/build/80754af9/greenlet_1628887725296/work
grpcio==1.41.0
gunicorn @ file:///home/conda/feedstock_root/build_artifacts/gunicorn_1620734326877/work
h11==0.13.0
h5py==3.1.0
HeapDict==1.0.1
html5lib @ file:///tmp/build/80754af9/html5lib_1593446221756/work
huepy==1.2.1
hyperlink==21.0.0
hyperopt==0.2.7
idna @ file:///home/linux1/recipes/ci/idna_1610986105248/work
imageio @ file:///tmp/build/80754af9/imageio_1617700267927/work
imageio-ffmpeg==0.4.5
imagesize @ file:///home/ktietz/src/ci/imagesize_1611921604382/work
importlib-metadata @ file:///tmp/build/80754af9/importlib-metadata_1617874469820/work
importlib-resources @ file:///home/conda/feedstock_root/build_artifacts/importlib_resources_1627688952556/work
imutils==0.5.4
incremental==21.3.0
infinity @ file:///home/conda/feedstock_root/build_artifacts/infinity_1622383622831/work
inflection==0.5.1
iniconfig @ file:///home/linux1/recipes/ci/iniconfig_1610983019677/work
instabot==0.117.0
install==1.3.4
intervals @ file:///home/conda/feedstock_root/build_artifacts/intervals_1625223179549/work
intervaltree @ file:///tmp/build/80754af9/intervaltree_1598376443606/work
ipykernel==4.8.2
ipython @ file:///tmp/build/80754af9/ipython_1628243924020/work
ipython-genutils @ file:///tmp/build/80754af9/ipython_genutils_1606773439826/work
ipywidgets @ file:///tmp/build/80754af9/ipywidgets_1610481889018/work
iso8601 @ file:///home/conda/feedstock_root/build_artifacts/iso8601_1626457081380/work
isort @ file:///tmp/build/80754af9/isort_1628603791788/work
itemadapter==0.4.0
itemloaders==1.0.4
itsdangerous @ file:///tmp/build/80754af9/itsdangerous_1621432558163/work
jdcal==1.4.1
jedi @ file:///tmp/build/80754af9/jedi_1606932564285/work
jeepney @ file:///tmp/build/80754af9/jeepney_1627537048313/work
Jinja2 @ file:///tmp/build/80754af9/jinja2_1612213139570/work
jmespath==0.10.0
joblib @ file:///tmp/build/80754af9/joblib_1613502643832/work
Js2Py==0.71
json-merge-patch==0.2
json5 @ file:///tmp/build/80754af9/json5_1624432770122/work
jsonpickle==2.1.0
jsonschema @ file:///tmp/build/80754af9/jsonschema_1602607155483/work
jupyter==1.0.0
jupyter-client @ file:///tmp/build/80754af9/jupyter_client_1616770841739/work
jupyter-console @ file:///tmp/build/80754af9/jupyter_console_1616615302928/work
jupyter-core @ file:///tmp/build/80754af9/jupyter_core_1612213311222/work
jupyter-dash==0.4.0
jupyter-server @ file:///tmp/build/80754af9/jupyter_server_1616083640759/work
jupyterlab @ file:///tmp/build/80754af9/jupyterlab_1629124491270/work
jupyterlab-pygments @ file:///tmp/build/80754af9/jupyterlab_pygments_1601490720602/work
jupyterlab-server @ file:///tmp/build/80754af9/jupyterlab_server_1629224937070/work
jupyterlab-widgets @ file:///tmp/build/80754af9/jupyterlab_widgets_1609884341231/work
keras==2.6.0
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.2
keyboard==0.13.5
keyring @ file:///tmp/build/80754af9/keyring_1621524402652/work
kiwisolver @ file:///tmp/build/80754af9/kiwisolver_1612282420641/work
koalas==1.8.2
langcodes==3.3.0
layers==0.1.5
lazy-object-proxy @ file:///tmp/build/80754af9/lazy-object-proxy_1616526917483/work
libarchive-c @ file:///tmp/build/80754af9/python-libarchive-c_1617780486945/work
llvmlite==0.36.0
locket==0.2.1
lockfile==0.12.2
lxml @ file:///tmp/build/80754af9/lxml_1616443220220/work
m3u8==0.9.0
Mako @ file:///home/conda/feedstock_root/build_artifacts/mako_1629523042001/work
Markdown @ file:///home/conda/feedstock_root/build_artifacts/markdown_1614595805172/work
MarkupSafe @ file:///tmp/build/80754af9/markupsafe_1621528148836/work
marshmallow==2.19.5
marshmallow-enum @ file:///home/conda/feedstock_root/build_artifacts/marshmallow-enum_1602427317752/work
marshmallow-sqlalchemy @ file:///home/conda/feedstock_root/build_artifacts/marshmallow-sqlalchemy_1623007433413/work
matplotlib @ file:///tmp/build/80754af9/matplotlib-suite_1628003481308/work
matplotlib-inline @ file:///tmp/build/80754af9/matplotlib-inline_1628242447089/work
matplotlib-venn==0.11.6
mccabe==0.6.1
metakernel==0.27.5
mistune==0.8.4
mkl-fft==1.3.0
mkl-random @ file:///tmp/build/80754af9/mkl_random_1626186064646/work
mkl-service==2.4.0
mock @ file:///tmp/build/80754af9/mock_1607622725907/work
more-itertools @ file:///tmp/build/80754af9/more-itertools_1622818384463/work
MouseInfo==0.1.3
moviepy==1.0.3
mpmath==1.2.1
msgpack @ file:///tmp/build/80754af9/msgpack-python_1612287151062/work
multipledispatch==0.6.0
munkres==1.1.4
murmurhash==1.0.6
music21==7.1.0
mypy-extensions==0.4.3
natsort @ file:///home/conda/feedstock_root/build_artifacts/natsort_1611580267114/work
navigator-updater==0.2.1
nbclassic @ file:///tmp/build/80754af9/nbclassic_1616085367084/work
nbclient @ file:///tmp/build/80754af9/nbclient_1614364831625/work
nbconvert @ file:///tmp/build/80754af9/nbconvert_1624479060632/work
nbformat @ file:///tmp/build/80754af9/nbformat_1617383369282/work
nest-asyncio @ file:///tmp/build/80754af9/nest-asyncio_1613680548246/work
networkx @ file:///tmp/build/80754af9/networkx_1627459939258/work
nltk @ file:///tmp/build/80754af9/nltk_1621347441292/work
nose @ file:///tmp/build/80754af9/nose_1606773131901/work
notebook @ file:///tmp/build/80754af9/notebook_1629205607169/work
num2words==0.5.10
numba @ file:///tmp/build/80754af9/numba_1616774046117/work
numexpr @ file:///tmp/build/80754af9/numexpr_1618856167419/work
numpy==1.22.3
numpydoc @ file:///tmp/build/80754af9/numpydoc_1605117425582/work
oauthlib==3.1.1
olefile==0.46
opencv-contrib-python==4.5.4.60
openpyxl @ file:///tmp/build/80754af9/openpyxl_1615411699337/work
opt-einsum==3.3.0
orderedmultidict==1.0.1
outcome==1.1.0
packaging==21.3
pandas==1.4.1
pandas-bokeh==0.5.5
pandas-datareader==0.10.0
pandocfilters @ file:///tmp/build/80754af9/pandocfilters_1605120460739/work
parsel==1.6.0
parso==0.7.0
partd @ file:///tmp/build/80754af9/partd_1618000087440/work
passlib==1.7.4
path @ file:///tmp/build/80754af9/path_1623603875173/work
pathlib2 @ file:///tmp/build/80754af9/pathlib2_1625585678054/work
pathspec==0.7.0
pathtools==0.1.2
pathy==0.6.1
patsy==0.5.1
pendulum @ file:///home/conda/feedstock_root/build_artifacts/pendulum_1604872503530/work
pep8==1.7.1
pexpect @ file:///tmp/build/80754af9/pexpect_1605563209008/work
phonenumbers @ file:///home/conda/feedstock_root/build_artifacts/phonenumbers_1631471427865/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow @ file:///tmp/build/80754af9/pillow_1625655817137/work
pipwin==0.5.1
pkginfo==1.7.1
platformdirs==2.4.0
plotly==5.3.1
pluggy @ file:///tmp/build/80754af9/pluggy_1615976321666/work
ply==3.11
preshed==3.0.6
prison @ file:///home/conda/feedstock_root/build_artifacts/prison_1631689776410/work
proglog==0.1.9
prometheus-client @ file:///tmp/build/80754af9/prometheus_client_1623189609245/work
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1616415428029/work
Protego==0.2.1
protobuf==3.18.0
psutil @ file:///tmp/build/80754af9/psutil_1612298023621/work
psycopg2-binary==2.9.3
ptyprocess @ file:///tmp/build/80754af9/ptyprocess_1609355006118/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
py @ file:///tmp/build/80754af9/py_1607971587848/work
py4j==0.10.9
pyarrow==5.0.0
pyasn1==0.4.8
pyasn1-modules==0.2.8
PyAudio==0.2.11
PyAutoGUI==0.9.53
pycodestyle @ file:///home/ktietz/src/ci_mi/pycodestyle_1612807597675/work
pycosat==0.6.3
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
pycurl==7.43.0.6
pydantic==1.8.2
PyDispatcher==2.0.5
pydocstyle @ file:///tmp/build/80754af9/pydocstyle_1621600989141/work
pydot==1.4.2
pydotplus==2.0.2
pyerfa @ file:///tmp/build/80754af9/pyerfa_1621560806183/work
pyflakes @ file:///home/ktietz/src/ci_ipy2/pyflakes_1612551159640/work
PyGetWindow==0.0.9
Pygments @ file:///tmp/build/80754af9/pygments_1629234116488/work
pyjsparser==2.7.1
PyJWT @ file:///home/conda/feedstock_root/build_artifacts/pyjwt_1619620644068/work
pylint @ file:///tmp/build/80754af9/pylint_1627536788098/work
pyls-black @ file:///tmp/build/80754af9/pyls-black_1607553132291/work
pyls-spyder @ file:///tmp/build/80754af9/pyls-spyder_1613849700860/work
PyMsgBox==1.0.9
pyodbc===4.0.0-unsupported
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1608057966937/work
pyparsing @ file:///home/linux1/recipes/ci/pyparsing_1610983426697/work
pyperclip==1.8.2
PyPrind==2.11.3
PyRect==0.1.4
pyrsistent @ file:///tmp/build/80754af9/pyrsistent_1600141720057/work
PyScreeze==0.1.28
pySmartDL==1.3.4
PySocks @ file:///tmp/build/80754af9/pysocks_1605305779399/work
pyspark==3.1.2
pytesseract==0.3.9
pytest==6.2.4
python-daemon @ file:///home/conda/feedstock_root/build_artifacts/python-daemon_1620244428749/work
python-dateutil==2.8.2
python-jsonrpc-server @ file:///tmp/build/80754af9/python-jsonrpc-server_1600278539111/work
python-language-server @ file:///tmp/build/80754af9/python-language-server_1607972495879/work
python-Levenshtein==0.12.2
python3-openid @ file:///home/conda/feedstock_root/build_artifacts/python3-openid_1612988842750/work
python3-xlib==0.15
pyttsx3==2.90
pytweening==1.0.4
pytz==2021.3
pytzdata @ file:///home/conda/feedstock_root/build_artifacts/pytzdata_1594644346367/work
PyWavelets @ file:///tmp/build/80754af9/pywavelets_1601658317819/work
pywhatkit==5.3
pyxdg @ file:///tmp/build/80754af9/pyxdg_1603822279816/work
PyYAML==5.4.1
pyzmq==17.0.0
QDarkStyle==2.8.1
QtAwesome @ file:///tmp/build/80754af9/qtawesome_1615991616277/work
qtconsole @ file:///tmp/build/80754af9/qtconsole_1623278325812/work
QtPy==1.9.0
Quandl==3.6.2
queuelib==1.6.2
regex @ file:///tmp/build/80754af9/regex_1629301332491/work
requests @ file:///tmp/build/80754af9/requests_1608241421344/work
requests-file==1.5.1
requests-oauthlib==1.3.0
requests-toolbelt==0.9.1
responses==0.18.0
retrying==1.3.3
rope @ file:///tmp/build/80754af9/rope_1623703006312/work
rsa==4.7.2
Rtree @ file:///tmp/build/80754af9/rtree_1618420845272/work
ruamel-yaml-conda @ file:///tmp/build/80754af9/ruamel_yaml_1616016699510/work
schedule==1.1.0
scikit-image==0.18.1
scikit-learn @ file:///tmp/build/80754af9/scikit-learn_1621370412049/work
scipy @ file:///tmp/build/80754af9/scipy_1618855647378/work
Scrapy==2.6.1
screeninfo==0.8
seaborn @ file:///tmp/build/80754af9/seaborn_1629307859561/work
SecretStorage @ file:///tmp/build/80754af9/secretstorage_1614022784285/work
selenium==4.1.2
Send2Trash @ file:///tmp/build/80754af9/send2trash_1607525499227/work
service-identity==21.1.0
setproctitle @ file:///home/conda/feedstock_root/build_artifacts/setproctitle_1611604428868/work
simplegeneric==0.8.1
singledispatch @ file:///tmp/build/80754af9/singledispatch_1629321204894/work
sip==4.19.13
six @ file:///home/conda/feedstock_root/build_artifacts/six_1620240208055/work
sklearn==0.0
smart-open==5.2.1
sniffio @ file:///tmp/build/80754af9/sniffio_1614030475067/work
snowballstemmer @ file:///tmp/build/80754af9/snowballstemmer_1611258885636/work
sortedcollections @ file:///tmp/build/80754af9/sortedcollections_1611172717284/work
sortedcontainers @ file:///tmp/build/80754af9/sortedcontainers_1623949099177/work
soupsieve @ file:///tmp/build/80754af9/soupsieve_1616183228191/work
spacy==3.2.1
spacy-legacy==3.0.8
spacy-loggers==1.0.1
SpeechRecognition==3.8.1
Sphinx @ file:///tmp/build/80754af9/sphinx_1623884544367/work
sphinxcontrib-applehelp @ file:///home/ktietz/src/ci/sphinxcontrib-applehelp_1611920841464/work
sphinxcontrib-devhelp @ file:///home/ktietz/src/ci/sphinxcontrib-devhelp_1611920923094/work
sphinxcontrib-htmlhelp @ file:///tmp/build/80754af9/sphinxcontrib-htmlhelp_1623945626792/work
sphinxcontrib-jsmath @ file:///home/ktietz/src/ci/sphinxcontrib-jsmath_1611920942228/work
sphinxcontrib-qthelp @ file:///home/ktietz/src/ci/sphinxcontrib-qthelp_1611921055322/work
sphinxcontrib-serializinghtml @ file:///tmp/build/80754af9/sphinxcontrib-serializinghtml_1624451540180/work
sphinxcontrib-websupport @ file:///tmp/build/80754af9/sphinxcontrib-websupport_1597081412696/work
spyder @ file:///tmp/build/80754af9/spyder_1616775618138/work
spyder-kernels @ file:///tmp/build/80754af9/spyder-kernels_1614030590686/work
spylon==0.3.0
spylon-kernel==0.4.1
SQLAlchemy @ file:///home/conda/feedstock_root/build_artifacts/sqlalchemy_1612225077951/work
SQLAlchemy-JSONField==0.9.0
sqlalchemy-orm==1.2.2
SQLAlchemy-Utils @ https://files.pythonhosted.org/packages/bc/e9/0815b06efd474bd13945c1975c33d3b7fe1200700f394924e75e018e6843/SQLAlchemy_Utils-0.37.0-py2.py3-none-any.whl
srsly==2.4.2
statsmodels @ file:///tmp/build/80754af9/statsmodels_1614023746358/work
stop-words==2018.7.23
sympy @ file:///tmp/build/80754af9/sympy_1618252284338/work
tables==3.6.1
tabulate @ file:///home/conda/feedstock_root/build_artifacts/tabulate_1614001031686/work
tblib @ file:///tmp/build/80754af9/tblib_1597928476713/work
tenacity @ file:///home/conda/feedstock_root/build_artifacts/tenacity_1626090218611/work
tensorboard==2.6.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.0
tensorflow==2.6.0
tensorflow-estimator==2.6.0
termcolor==1.1.0
terminado==0.9.4
tesseract==0.1.3
test-helper==0.2
testpath @ file:///tmp/build/80754af9/testpath_1624638946665/work
text-unidecode==1.3
textdistance @ file:///tmp/build/80754af9/textdistance_1612461398012/work
thinc==8.0.13
threadpoolctl @ file:///Users/ktietz/demo/mc3/conda-bld/threadpoolctl_1628668762525/work
three-merge @ file:///tmp/build/80754af9/three-merge_1607553261110/work
thrift @ file:///home/conda/feedstock_root/build_artifacts/thrift_1614958301229/work
tifffile==2020.10.1
tldextract==3.2.0
toml @ file:///tmp/build/80754af9/toml_1616166611790/work
toolz @ file:///home/linux1/recipes/ci/toolz_1610987900194/work
tornado @ file:///tmp/build/80754af9/tornado_1606942300299/work
tqdm @ file:///tmp/build/80754af9/tqdm_1629302309755/work
traitlets @ file:///home/ktietz/src/ci/traitlets_1611929699868/work
treelib==1.6.1
trio==0.20.0
trio-websocket==0.9.2
Twisted==22.2.0
typed-ast @ file:///tmp/build/80754af9/typed-ast_1610484547928/work
typer==0.4.0
typing-inspect==0.7.1
typing_extensions @ file:///home/conda/feedstock_root/build_artifacts/typing_extensions_1637155965157/work
tzlocal @ file:///home/conda/feedstock_root/build_artifacts/tzlocal_1629721593796/work
ua-parser==0.10.0
ujson @ file:///tmp/build/80754af9/ujson_1611259522456/work
unicodecsv==0.14.1
Unidecode==1.3.2
urllib3 @ file:///tmp/build/80754af9/urllib3_1625084269274/work
vaderSentiment==3.3.2
vineyard==0.3.16
virtualenv==20.10.0
w3lib==1.22.0
wasabi==0.9.0
watchdog @ file:///tmp/build/80754af9/watchdog_1612471027849/work
wcwidth @ file:///tmp/build/80754af9/wcwidth_1593447189090/work
webcolors==1.11.1
webdriver-manager==3.5.3
webencodings==0.5.1
Werkzeug @ file:///home/conda/feedstock_root/build_artifacts/werkzeug_1621518206714/work
wget==3.2
widgetsnbextension==3.5.1
wikipedia==1.4.0
wordcloud==1.8.1
wrapt==1.12.1
wsproto==1.1.0
WTForms @ file:///home/conda/feedstock_root/build_artifacts/wtforms_1596060166590/work
wurlitzer @ file:///tmp/build/80754af9/wurlitzer_1626947794172/work
xlrd @ file:///tmp/build/80754af9/xlrd_1608072521494/work
XlsxWriter @ file:///tmp/build/80754af9/xlsxwriter_1628603415431/work
xlwt==1.3.0
xmltodict==0.12.0
yapf @ file:///tmp/build/80754af9/yapf_1615749224965/work
zict==2.0.0
zipp @ file:///tmp/build/80754af9/zipp_1625570634446/work
zope.deprecation==4.4.0
zope.event==4.5.0
zope.interface @ file:///tmp/build/80754af9/zope.interface_1625035545636/work
