from dash import dcc, html
from dash_iconify import DashIconify

from app import app
from components.componentes import header, footer
import components.callbacks as callbacks


app.layout = html.Div([
    dcc.Location(id='url', refresh=False),     
    html.Div([
        header(),    
        dcc.Store(id = 'store'),
        ##### Contenido de las páginas #####        
        html.Div(style = {'position': 'absolute','left': '0px', 'top': '35px', 'width': '100%', 'height':'100%'},
                 id='page-content'),
        ##### Icono coche #####
        html.Div(style = {'position': 'absolute','right': '10px', 'top': '0px'},id = 'btn_menu', n_clicks = 0, children = [
            DashIconify(
                icon="bx:car",
                width=30),   
        #### Caja de páginas #####
        html.Div(style = {'position': 'fixed','right': '15px', 'top': '30px', 'padding': '1px',
                          'border-radius': '4px', 'background': '#D1F2EB', 'text-align': 'center'}, id = 'caja', className = '', children = [])         
        ]),
        footer()
    ])
])
        

if __name__ == '__main__':
    app.run_server(debug=True)


