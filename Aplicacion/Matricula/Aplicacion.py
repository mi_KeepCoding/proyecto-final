from Matricula.detecta_matricula import *
from Matricula.devuelve_matricula import * 
import requests
import xmltodict
import json
import pandas as pd
import shutil
from PIL import Image

def foto(foto):

    def get_vehicle_info(plate_number):
        print(plate_number)
        r = requests.get(f"http://www.regcheck.org.uk/api/reg.asmx/CheckSpain?RegistrationNumber={plate_number}&username=eluse71&password=alexacar")
        data = xmltodict.parse(r.content) 
        jdata = json.dumps(data)                                         # convert to json      
        df = json.loads(jdata)                                           # convert json to dataframe
        df1 = json.loads(df['Vehicle']['vehicleJson'])                   # convert json to dataframe
        return df1
 
    try:               
        save_path = f'Matricula/Fotos/{foto}'                            # Ruta donde se guardaran las fotos
        char = segment_characters(plate_detect(cv2.imread(save_path)))   # Segmenta los caracteres de la matricula
        plate_number = show_results(char)                                # Muestra los caracteres de la matricula   
        
        if len(plate_number) > 7:                                        # Si la matricula es mayor a 7 caracteres
            plate_number = plate_number[1:]                              # Se elimina el primer caracter
        

        #shutil.copyfile(save_path, 'assets/coche.jpg')                   # Se copia la foto a la carpeta assets 
        
        img = Image.open(save_path)
        new_img = img.resize((700,400))
        new_img.save('assets/coche.jpg','png')

        datos = get_vehicle_info(plate_number)   # Obtiene los datos del coche
        potencia = datos['Variation']            # Potencia del coche 
        
        for x in potencia.split(' '): 
            try:
                if int(x) > 49:                                         # Si la potencia es mayor a 49    
                    potencia = int(x)                                   # Se guarda la potencia
            except:
                pass                                                    # Si no es un numero se pasa
        
        # Se guardan los datos en un diccionario
        respuesta = {'Matricula' : plate_number,
                     'Marca': [datos['CarMake']['CurrentTextValue'].upper()],
                     'Modelo': [datos['CarModel']['CurrentTextValue'].lower()],
                     'CC': [str(round(int(datos['EngineSize'])/1000,1))],
                     'Potencia':[potencia],
                     'Combustible': [datos['Fuel'].lower()],
                     'Anyo': [2022 - int(datos['RegistrationYear'])],
                     'Puertas': [int(datos['Doors'])]}     
        
        respuesta = pd.DataFrame(respuesta)                            # Se convierte el diccionario en un dataframe
        print(respuesta)
        respuesta.to_csv('Matricula/data/data.csv')                    # Se guarda el dataframe en un csv
    except:
        pass






