from keras.models import load_model
import cv2
import numpy as np

def f1score(y, y_pred):
    return f1_score(y, tf.math.argmax(y_pred, axis=1), average='micro') 

def custom_f1score(y, y_pred):
    return tf.py_function(f1score, (y, y_pred), tf.double) 

model = load_model('Matricula/Modelo/plate.pkl', custom_objects= {'custom_f1score': custom_f1score}) #

def fix_dimension(img):
    new_img = np.zeros((28,28,3))   #crear una matriz de ceros para almacenar la imagen
    for i in range(3):              #recorrer las 3 dimensiones de la imagen
        new_img[:,:,i] = img        #copiar la imagen en la matriz de ceros
    return new_img
  
def show_results(char): 
    dic = {}
    characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for i,c in enumerate(characters):
        dic[i] = c
        
    output = []
    for i,ch in enumerate(char): 
        img_ = cv2.resize(ch, (28,28), interpolation=cv2.INTER_AREA)
        img = fix_dimension(img_)
        img = img.reshape(1,28,28,3)
        y_ = model.predict(img)[0]   
        output.append(dic[np.where(y_ == np.amax(y_))[0][0]])       
          
    plate_number = ''.join(output)
    
    return plate_number
