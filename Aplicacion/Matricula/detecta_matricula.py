import numpy as np
import cv2
import matplotlib.pyplot as plt

#detección de la matrícula del vehículo
plateCascade = cv2.CascadeClassifier('Matricula/Modelo/plate.xml')

def plate_detect(img):  
     #detectar la placa
    roi = img.copy()  
    for (x,y,w,h) in plateCascade.detectMultiScale(roi,
                                                   scaleFactor = 1.2,
                                                   minNeighbors = 7):  
        roi_ = roi[y:y+h, x:x+w, :]  # recortar la imagen
    return roi[y:y+h, x:x+w, :]      # recortar la imagen 

def segment_characters(image) :
    
    def find_contours(dimensions, img) :          
        #Comprobar los 15 contornos más grandes para el carácter de la matrícula respectivamente
        cntrs = sorted(cv2.findContours(img.copy(),
                                        cv2.RETR_TREE, 
                                        cv2.CHAIN_APPROX_SIMPLE)[0],
                                        key=cv2.contourArea,reverse=True)[:15]   
        #Dimensiones aproximadas de los contornos
        lower_width, upper_width, lower_height, upper_height = dimensions       
        x_cntr_list = []
        img_res = [] # Lista que almacena la imagen binaria (sin clasificar)
        
        for cntr in cntrs :            
            intX, intY, intWidth, intHeight = cv2.boundingRect(cntr) #coordenadas del rectángulo que lo encierra      
            
            if (intWidth > lower_width and intWidth < upper_width 
                and intHeight > lower_height and intHeight < upper_height): #comprobar las dimensiones del contorno
                x_cntr_list.append(intX) #guardar las coordenadas de los contornos
                char = np.zeros((44,24)) #crear una matriz de ceros para almacenar el carácter
                
                #extrayendo cada carácter utilizando las coordenadas del rectángulo que lo rodea.  
                cv2.rectangle(cv2.imread('contorno.jpg'),  # imagen de contorno
                             (intX,intY),      
                             (intWidth+intX,
                              intY+intHeight), 
                             (50,21,200), 2)       

                char[2:42, 2:22] = cv2.subtract(255, cv2.resize(img[intY:intY+intHeight,
                                                                intX:intX+intWidth],(20, 40))) # imagen binaria  

                char[0:2, :] = char[:, 0:2] = char[42:44, :] = char[:, 22:24] = 0 #borrar los bordes               
                img_res.append(char) 

        #devuelve los caracteres en orden ascendente con respecto a la coordenada x       
        return np.array([img_res[idx] for idx in sorted(range(len(x_cntr_list)),
                                                       key=lambda k: x_cntr_list[k])]) #ordenar los caracteres por x

    """pre-procesamiento de la imagen recortada de la placa
       Umbral: convertir a blanco y negro puro con bordes nítidos
       erod: aumentar el negro del fondo
       dilate: aumentar el blanco de la placa"""    
    img_gray_lp = cv2.cvtColor(cv2.resize(image, (333, 75)), 
                               cv2.COLOR_BGR2GRAY) 

    img_binary_lp = cv2.threshold(img_gray_lp, 200, 255,
                                  cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]  #umbralizar

    img_binary_lp = cv2.erode(img_binary_lp, (3,3))                      # erode
    img_binary_lp = cv2.dilate(img_binary_lp, (3,3))                     # aumentar el blanco de la placa

    LP_WIDTH, LP_HEIGHT = img_binary_lp.shape
    dimensions = [LP_WIDTH/6, LP_WIDTH/2, LP_HEIGHT/10, 2*LP_HEIGHT/3]
    img_binary_lp[0:3,:] = img_binary_lp[:,0:3] = 255                    #borrar los bordes
    img_binary_lp[72:75,:] = img_binary_lp[:,330:333] = 255              #borrar los bordes    

    return find_contours(dimensions, img_binary_lp)
