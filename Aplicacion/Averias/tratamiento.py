from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType,IntegerType
import pandas as pd

def tratamiento():

    marcas = {'ROVER': 1, 'LAMBORGHINI': 2, 'PORSCHE': 3, 'DR': 4, 'HYUNDAI': 5,
                'DACIA': 6, 'ASTON': 7, 'FIAT': 8, 'KARMA': 9, 'TOYOTA': 10,
                'SKODA': 11, 'SUBARU': 12, 'NISSAN': 13, 'CITROEN': 14, 'AUDI': 15,
                'BENTLEY': 16, 'SUZUKI': 17, 'INFINITI': 18, 'FORD': 19, 'SEAT': 20,
                'FERRARI': 21, 'CUPRA': 22, 'MINI': 23, 'SAAB': 24, 'PEUGEOT': 25,
                'DODGE': 26, 'ABARTH': 27, 'CHEVROLET': 28, 'JAGUAR': 29, 'ALFA': 30,
                'SANGYONG': 31, 'LEXUS': 32, 'JEEP': 33, 'VOLVO': 34, 'MCLAREN': 35,
                'TESLA': 36, 'MAZDA': 37, 'DAEWOO': 38, 'BMW': 39, 'MASERATTI': 40, 
                'MG': 41, 'VOLKSWAGEN': 42, 'DAIMLER': 43, 'KIA': 44, 'HUMMER': 45,
                'HONDA': 46, 'SMART': 47, 'CADILLAC': 48, 'RENAULT': 49, 'DS': 50,
                'ROLLS ROYCE': 51, 'PLYMOUTH': 52, 'LOTUS': 53, 'OPEL': 54, 'LANCIA': 55,
                'MERCEDES': 56, 'KTM': 57, 'DFSK': 58, 'CHRYSLER': 59, 'MITSUBITSI': 60}

    def marca(string):
        if string in marcas:
            return string
    udfMarca = F.udf(marca, IntegerType())       

    def averia(string):
        try:        
            if 'FRENOS' in string or 'FRENO' in string:
                return 'FRENOS'
            elif 'ALTERNADOR' in string:
                return 'ALTERNADOR'
            elif 'CARBURADOR' in string:
                return 'CARBURADOR'
            elif 'AMORTIGUADOR' in string:
                return 'AMORTIGUADOR'
            elif 'EMISIONES' in string:
                return 'EMISIONES'
            elif 'ESCAPE' in string:
                return 'ESCAPE'
            elif 'CILINDROS' in string:
                return 'CILINDROS'
            elif 'ARRANQUE' in string:
                return 'MOTOR DE ARRANQUE'
            elif 'DIRECCION' in string:
                return 'DIRECCION'
            elif 'EMBRAGUE' in string:
                return 'EMBRAGUE'
            elif 'ABC MOTOR' in string:
                return 'ABC MOTOR'
            elif 'ABC MOTOR' in string:
                return 'ABC MOTOR'
            elif 'SISTEMA ELECTRICO' in string:
                return 'SISTEMA ELECTRICO'
            elif 'RADIADOR' in string or 'RADIADORES' in string:
                return 'RADIADOR'
            elif 'CORREA' in string:
                return 'CORREA'            
        except:
            pass
    udfAveria = F.udf(averia, StringType())  

    spark = SparkSession\
            .builder\
            .appName("Pipeline")\
            .getOrCreate()

    df = spark.read.csv('Averias/Datasets/datostalleres2010.csv',inferSchema=True,
                        header =True, encoding='latin1')[['MARCA_ID', 'ANIO_VEHICULO',  
                                                        'KILOMETRAJE', 'DESCRIPCION']]

    df = (df.withColumn('MARCA', udfMarca(F.col('MARCA_ID')))
        .drop('MARCA_ID')
        .na.drop("any")
        .withColumn('AVERIA', udfAveria(F.col('DESCRIPCION')))
        .drop('DESCRIPCION')
        .na.drop("any")
        .withColumn('ANIO_VEHICULO', 2022 - F.col('ANIO_VEHICULO'))
        .withColumn('KILOMETRAJE', F.col('KILOMETRAJE').cast('int'))
        .na.drop("any")
        .drop_duplicates()
        )

    df.toPandas().to_csv('Averias/Datasets/datostalleres2010_tratado.csv')