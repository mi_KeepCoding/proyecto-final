from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
from joblib import dump

def modelo():
    df = pd.read_csv('Averias/Datasets/datostalleres2010_tratado.csv', encoding='latin1')[['ANIO_VEHICULO', 'KILOMETRAJE', 'MARCA', 'AVERIA']]

    train, test = train_test_split(df, test_size=0.2, shuffle=True, random_state=0)
    print('Dimensiones del dataset de training:', train.shape)
    print('Dimensiones del dataset de test:', test.shape)

    y_train = train['AVERIA'] 
    X_train = train.loc[:, test.columns != 'AVERIA']    
        
    y_test = test['AVERIA'] 
    X_test = test.loc[:, test.columns != 'AVERIA']    

    scaler = preprocessing.StandardScaler().fit(X_train)
    XtrainScaled = scaler.transform(X_train)
    XtestScaled = scaler.transform(X_test)  

    maxDepthOptimo = 6
    modelo = RandomForestClassifier(max_depth=maxDepthOptimo).fit(XtrainScaled, y_train)
    print("Accuracy en train:", modelo.score(XtrainScaled, y_train))
    print("Accuracy en test:", modelo.score(XtestScaled, y_test))  
    
    dump(scaler, 'Averias/Modelos/std_scaler.bin', compress=True)  
    dump(modelo, 'Averias/Modelos/modelo_entrenado.pkl')


