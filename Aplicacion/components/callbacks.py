from app import app
from dash import dcc, html
from dash.dependencies import Input, Output, State
import base64
from datetime import datetime
from os import remove
import plotly.express as px
from statistics import mode
from turtle import color
import plotly.graph_objects as go
import wget

from components.mform import formulario, bar, buscador
from components.listas import modelos, columnas_df, graficos, marcas
from components.componentes import leer_csv, guardar_csv, copia_fichero, mueve_fichero, actualizar_csv, dataTable, selector_outlayer
from Prediccion.prediccion import prediccion
from Matricula import Aplicacion 
import pages




################################################################################################################################
########################################  Index   ##############################################################################
################################################################################################################################

@app.callback([Output('caja', 'children'),Output('caja','className')],
               Input('btn_menu', 'n_clicks'), 
               State('caja','className'),prevent_initial_call = True)
def display_page(click,caja):

    if caja == '':
        obj = buscador()
        return [obj,'ok']
    if caja == 'ok':
        return [[],''] 

################ Update page content #############################
@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/inicio':
        return pages.inicio.layout    
    elif pathname == '/outlayers':
        return pages.outlayers.layout
    elif pathname == '/HistoricoConsultas':
        return pages.historico_consultas.layout
    elif pathname == '/averias':
        return pages.averias.layout
    elif pathname == '/Spark':
        return pages.spark.layout   
    elif pathname == '/dashboard':
        return pages.pagina4.layout 
    else:
        return pages.portada.layout

################ Seleccionar página ##############################
@app.callback([Output('url', 'pathname'),Output('pagina', 'value')],
             Input('pagina', 'value'),prevent_initial_call = True)
def display_page(value):
    return [f'/{value.replace(" ","")}',value]

################################################################################################################################
########################################  Página Estimación Precio  ############################################################
################################################################################################################################

##### Selecciona entre formulario con datos de matricula o con datos a mano ####################################
@app.callback([Output('formulario1','children'),Output('formulario2','children'),
               Output('formulario1','style'),Output('formulario2','style'),
               Output('actualizar','children'),Output('div_prediccion','style')],
               Input('actualizar','n_clicks'),State('actualizar','children'),prevent_initial_call = True)
def actualizar(click,nombre): 
   
    if click > 0:
        if nombre == 'Introducir Datos'  :                                        #button introducir_vehiculo        
            return [formulario(), [], {'position': 'absolute','left': '10px', 'top': '10px','width': '270px', 'height': '680px','background': '#068EB6'}, 
                    {},'Vehiculo Entrante', {'position': 'absolute','left': '291px', 'top': '624px', 'width': '115px', 'height': '64px', 'background': '#068EB6'}]                                   
        else:                    #button actualizar formulario con datos de matricula        
            return  [[], formulario(1), {}, {'position': 'absolute','left': '10px', 'top': '10px','width': '270px', 'height': '720px','background': '#068EB6'}, 
                     'Introducir Datos', {'position': 'absolute','left': '291px', 'top': '664px', 'width': '115px', 'height': '64px', 'background': '#068EB6'}]
    else:
        return [formulario(), [], {'position': 'absolute','left': '10px', 'top': '10px','width': '270px', 'height': '680px','background': '#068EB6'}, {},
                'Vehiculo Entrante',{'position': 'absolute','left': '291px', 'top': '624px', 'width': '115px', 'height': '64px', 'background': '#068EB6'}]

       
### Actualiza modelo de coche por la marca seleccionada 
@app.callback(
    Output('modelo', 'options'),
    Input('marca', 'value'))
def update_modelo(marca): 
   
    try:
        return sorted(modelos[marca])  
    except:
        return []

################ Botones de opciones y tabla #################################################
@app.callback(
    [Output('tabla', 'children'), Output('limpiar', 'n_clicks'),
     Output('actualizar', 'n_clicks'),Output('precio_marca', 'value'),
     Output('precio_anyo', 'value'), Output('precio_potencia', 'value'),
     Output('precio_kilometros', 'value'), Output('div_limpiar', 'children')],
    [Input('limpiar', 'n_clicks'), Input('actualizar', 'n_clicks'),
     Input('precio_marca', 'value'), Input('precio_anyo', 'value'),
     Input('precio_potencia', 'value'),Input('precio_kilometros', 'value')],
    [State('tabla', 'children'), State('orden', 'value'),
     State('actualizar', 'children')],prevent_initial_call = True)
def tabla(limpiar, actu, marca, anyo, potencia, kilometros, tabla, orden, nombre):
    
    ordenado = True
    if (orden == 'Primero mas alto'): ordenado = False
    
    df = leer_csv('Datasets/dataset.csv').sort_values('Precio', ascending = ordenado)
    df['Años'] = df['Anyo']
    df = df[['Precio', 'Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Años', 'Puertas', 'Cambio', 'Color', 'Metalizado']]

    if limpiar is not None:
        if limpiar > 0:
            return [[],0,0,[],[],[],[],html.Td(id = 'limpiar')]

    if actu > 0 and nombre == 'Vehiculo Entrante':
        encoded_image = base64.b64encode(open('assets/coche.jpg', 'rb').read())
        return [[html.Div(id = 'tabla', style = {'width': '1500px', 'height': '580px'},children = [
                    html.Img(style = {'margin-left': '150px'}, src='data:image/png;base64,{}'.format(encoded_image.decode()))])],
                limpiar, actu,[],[],[],[],html.Td(id = 'limpiar')] 

    if type(marca) == str:
        df = df[df['Marca'] == marca]
    
    if type(anyo) == int:        
        df = df[df['Años'] == anyo]

    if type(potencia) == int:        
        if potencia < 150: potencia2 = potencia + 10
        else: potencia2 = potencia + 50
        df = df[df['Potencia'] >= potencia]
        df = df[df['Potencia'] < potencia2]

    if type(kilometros) == int:        
        kilometros2 = kilometros + 5000
        df = df[df['Kilometros'] >= kilometros]
        df = df[df['Kilometros'] < kilometros2]

    if (type(marca) != str and type(anyo) != int and type(potencia) != int and type(kilometros) != int):
        return [[], 0, 0, marca, anyo, potencia, kilometros, html.Td(id = 'limpiar')] 


    return [[dataTable('data_table',df),
                html.Div(id='output_div')], 
                0, 0, marca, anyo, potencia, kilometros,
                html.Button(children = 'Eliminar Tabla', id = 'limpiar', n_clicks = 0, style = {'margin-top':'-50px', 'width': '300px', 'border': '1px solid black'})] 
 
### Boton para solicitar prediccion ##################################################################
@app.callback(
    [Output('prediccion', 'value'),Output('calcular','n_clicks')],
    [Input('calcular','n_clicks'), Input('actualizar','n_clicks')], 
    [State('marca','value'),State('modelo','value'),State('cc','value'),
     State('potencia','value'),State('combustible','value'),State('kilometros','value'),
     State('anyos','value'),State('puertas','value'),State('cambio','value'),
     State('color','value'),State('metalizado','value')],prevent_initial_call = True)
def calcular_prediccion(click, click2, marca,modelo,cc,potencia,combustible,kilometros,anyo,puertas,cambio,color,metalizado):  
           
    if click > 0:
        try:    
            predic = prediccion([marca,modelo,cc,potencia,combustible,kilometros,anyo,puertas,cambio,color,metalizado])  #prediccion
            actualizar_csv('Prediccion/Resultados/consultas.csv',
                            [marca,modelo,cc,potencia,combustible,kilometros,anyo,puertas,cambio,color,metalizado,predic,datetime.now()]) #actualizacion de csv        
            return [predic,0]
        except:
            return ['Faltan datos',0]

    return ['Estimación',0]

### Boton para activar aplicacion de matrícula #####################################################################           
@app.callback(
    Output('fotos','n_clicks'), Input('fotos','n_clicks'), State('foto','value'),prevent_initial_call = True)
def foto(click,path):
    if click > 0:
        Aplicacion.foto(path)        
    return 0 


#####################################################################################################################################################################
######################################## Página Búsqueda de Outlayers ###############################################################################################
#####################################################################################################################################################################
  
#################### Graficos
@app.callback(
    [Output('selector_histo', 'children'), Output('boton_histo', 'n_clicks')],
    [Input('boton_histo', 'n_clicks')],prevent_initial_call = True)
def selector_histograma(click):    
    if click == 1:         
        return [selector_outlayer('', 'valor_histo', columnas_df, '', False, width = '120px'), click]
    else:
        return [html.Td(id = 'valor_histo'), 0]   

@app.callback(
    [Output('tabla_histo', 'children'), Output('tabla_histo_div', 'style'),Output('busqueda_div', 'style'), 
     Output('selector_histo', 'style'), Output('layout', 'style'),
     Output('selector_grafico', 'children'), Output('busqueda_div', 'children')],
    [Input('valor_histo', 'value'), Input('valor_grafico', 'value')],
    [State('valor_grafico', 'value'), State('boton_histo', 'n_clicks')],prevent_initial_call = True)
def tabla_histograma(histo, grafico, state_graf, histo_click):      
    
    if histo_click == 1:
        if histo is not None and len(histo) > 0:  #si se ha seleccionado una columna

            df = leer_csv('Datasets/dataset_outlayers.csv')
            if df.columns[0] != 'Precio':
                df = df.iloc[:,1:] 
            fig = []  
            
            #Histograma
            if state_graf == 'Histograma':                
                fig = px.histogram(df, x = histo, nbins = 10)
            #Tarta
            if state_graf == 'Tarta':

                if histo == 'Precio': 
                    precios = []
                    for prec in df['Precio'].values:
                        if prec <= 5000:
                            precios.append('Entre 0 y 5000')
                        elif prec <= 10000:
                            precios.append('Entre 5000 y 10000')
                        elif prec <= 15000:
                            precios.append('Entre 10000 y 15000')
                        elif prec <= 20000:
                            precios.append('Entre 15000 y 20000')
                        elif prec <= 25000:
                            precios.append('Entre 20000 y 25000')
                        elif prec <= 30000:
                            precios.append('Entre 25000 y 30000')
                        elif prec > 30000:
                            precios.append('Mayor a 30000')
                    fig = px.pie(df, names = precios, hole=.3)  
                    
                elif histo == 'Potencia':
                    potencias = []
                    for pot in df['Potencia'].values:
                        if pot <= 50:
                            potencias.append('Entre 0 y 50')
                        elif pot <= 100:
                            potencias.append('Entre 50 y 100')
                        elif pot <= 150:
                            potencias.append('Entre 100 y 150')
                        elif pot <= 200:
                            potencias.append('Entre 150 y 200')
                        elif pot <= 250:
                            potencias.append('Entre 200 y 250')
                        elif pot <= 300:
                            potencias.append('Entre 250 y 300')
                        elif pot > 300:
                            potencias.append('Mayor a 300')
                    fig = px.pie(df, names = potencias, hole=.3)     

                elif histo == 'Kilometros':
                    kilometros = []
                    for kilo in df['Kilometros'].values:
                        if kilo <= 20000:
                            kilometros.append('Entre 0 y 20000')
                        elif kilo <= 40000:
                            kilometros.append('Entre 20000 y 40000')
                        elif kilo <= 60000:
                            kilometros.append('Entre 40000 y 60000')
                        elif kilo <= 80000:
                            kilometros.append('Entre 60000 y 80000')                        
                        elif kilo <= 100000:
                            kilometros.append('Entre 80000 y 100000')
                        elif kilo <= 150000:
                            kilometros.append('Entre 100000 y 150000')
                        elif kilo <= 200000:
                            kilometros.append('Entre 150000 y 200000')
                        elif kilo > 200000:
                            kilometros.append('Mayor a 200000')
                    fig = px.pie(df, names = kilometros, hole=.3)  

                elif histo == 'Anyo':
                    anyos = []
                    for anyo in df['Anyo'].values:
                        if anyo == 0:
                            anyos.append('0 años')
                        elif anyo == 1:
                            anyos.append('1 año')
                        elif anyo == 2:
                            anyos.append('2 años')
                        elif anyo == 3:
                            anyos.append('3 años')
                        elif anyo == 4:
                            anyos.append('4 años')
                        elif anyo == 5:
                            anyos.append('5 años')
                        elif anyo <= 10:
                            anyos.append('6 a 10 años')
                        elif anyo <= 15:
                            anyos.append('11 a 15 años')
                        elif anyo <= 20:
                            anyos.append('16 a 20 años')
                        elif anyo > 20:
                            anyos.append('Mayor a 20 años') 
                    fig = px.pie(df, names = anyos, hole=.3)    

                else:
                    fig = px.pie(df,  names = histo, hole=.3)     
                fig.update_traces(textposition='inside', textinfo='percent+label')   
            #Violin        
            if state_graf == 'Violin':
                fig = px.violin(df, x = histo)
            #Mapa de Calor
            if state_graf == 'Mapa de Calor':
                fig = px.density_heatmap(df, x=histo, y="Precio", nbinsx=20, nbinsy=20)
            #Scatter 
            if state_graf == 'Scatter':
                fig = px.scatter(df, x=histo, y="Kilometros", color="Anyo", size='Precio')
               
            return [html.Div([dcc.Graph(id='life-exp-vs-gdp', figure = fig)]),                                             #'tabla_histo', 'children'
                    {'position': 'absolute', 'top': '49px', 'left': '4px', 'width': '1100px', 'background': '#068EB6'},    #tabla_histo_div', 'style'
                    {'position': 'absolute', 'top': '504px', 'width': '1288px',},                                           #busqueda_div', 'style'
                    {'margin-top': '10px', 'margin-left': '20px', 'width': '150px', 'height': '53px'},                    #selector_histo', 'style'
                    {'position': 'relative','left': '20px', 'top': '10px', 'width': '1288px','height': '553px', 'background': '#C8D9DE'},   #'layout', 'style'
                    selector_outlayer('', 'valor_grafico', graficos, f'{state_graf}', False, width = '120px',
                                      pos = '160px', arr = '-53px'),                                                      #'selector_tarta', 'children'
                    [#boton
                    html.Button(children = 'Busqueda de Outlayers', id = 'boton_busqueda', n_clicks = 0,
                                style = {'margin': '4px','border': '1px solid black', 'width': '1278px', 'font-size': '30px',  
                                'color': 'white', 'background': '#068EB6'}),
                    #tabla               
                    html.Div(id = 'tabla_busqueda_div', style = {'background': '#C8D9DE'}, children =[
                        html.Div(style = {'margin-left': '8px', 'margin-top': '-20px'}, id = 'tabla_busqueda', children =[])
                    ]),
                    #Criterio                
                    html.Div(id = 'criterio_busqueda', children =[
                        dcc.Input(style = {'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '0px', 'height': '0px','border': '1px solid white'}, 
                                    id = 'valor_busqueda')])] ]  
            
        else:     
            return [[],{}, {'position': 'absolute', 'top': '67px'},
                    {'margin-left': '20px', 'width': '150px', 'height': '53px'},
                    {'position': 'relative','left': '20px', 'top': '10px', 'width': '1288px', 'height': '88px', 'background': '#C8D9DE'},
                    selector_outlayer('', 'valor_grafico', graficos, 'Histograma', False, width = '120px',
                                      pos = '160px', arr = '-53px'),
                    html.Div(style = {'position': 'absolute', 'top': '-100px', 'left': '-20px'}, id = 'criterio_busqueda', children =[
                        dcc.Input(style = {'width': '0px', 'height': '0px','border': '1px solid white'}, 
                                  id = 'valor_busqueda')])]
        

    return [[],{}, {'position': 'absolute', 'top': '28px'}, {},
            {'position': 'relative','left': '20px', 'top': '10px', 'width': '1288px', 'height': '30px', 'background': '#068EB6'},
             html.Td(id = 'valor_grafico'), 
             html.Div(style = {'position': 'absolute', 'top': '-60px', 'left': '-20px'}, id = 'criterio_busqueda', children =[
                            dcc.Input(style = {'width': '0px', 'height': '0px','border': '1px solid white'}, 
                                      id = 'valor_busqueda')])]


##############Busqueda
@app.callback(
    [Output('criterio_busqueda', 'children'), Output('boton_busqueda', 'n_clicks')],
    [Input('boton_busqueda', 'n_clicks')],[State('valor_histo','value')],prevent_initial_call = True)
def criterio_busqueda(click,histo): 

    if click == 1:  
        df = leer_csv('Datasets/dataset_outlayers.csv')        
        return [dcc.Dropdown(id = 'valor_busqueda',
                          style = {'position': 'absolute', 'top': '2.5px', 'left': '3px', 'width': '150px'}, 
                          options = sorted(list(df[histo].unique())), value = '', placeholder = 'Criterio de búsqueda'), click]
    else:
        return [dcc.Input(style = {'position': 'absolute', 'top': '-30px', 'left': '20px', 'width': '0px', 'height': '0px','border': '1px solid white'}, 
                          id = 'valor_busqueda'), 0]  

#Tabla Busqueda outlayers
@app.callback(Output('tabla_busqueda', 'children'),
              Input('valor_busqueda', 'value'),
              State('valor_histo', 'value'))
def tabla_busqueda(criterio, columna):
   
    if criterio is not None and criterio != '':
        df = leer_csv('Datasets/dataset_outlayers.csv').sort_values('Precio')
        
        if df.columns[0] != 'Precio':
            df = df.iloc[:,1:]

        df = df[df[columna] == criterio]
            
        return dataTable('data_table', df, alto = '120px', ancho = '1200px')
    return []

############## Eliminar outlayers ##############################
@app.callback(
    Output('valor_eliminar', 'value'),
    Input('aceptar_eliminar','n_clicks'),
    State('valor_eliminar', 'value'),prevent_initial_call = True)
def eliminar(click, valor):  

    df = leer_csv('Datasets/dataset_outlayers.csv')    
    if df.columns[0] != 'Precio':
        df = df.iloc[:,1:]

    columna = valor.split(',')[0]
    valor = valor.split(',')[1]
    
    if columna == 'Marca' or columna == 'Modelo' or columna == 'Color' :
        df = df.drop(df[df[columna] == valor].index)
    elif columna == 'CC':
        df = df.drop(df[df[columna] == float(valor)].index)  
    else:
        df = df.drop(df[df[columna] == int(valor)].index)      
   
    guardar_csv(df, 'Datasets/dataset_outlayers.csv')   
    return 'Columna,Criterio'

@app.callback(
    [Output('selector_eliminar', 'children'), Output('boton_eliminar', 'n_clicks'),
     Output('layout2', 'style')],
    [Input('boton_eliminar', 'n_clicks'),Input('valor_busqueda', 'value'),
     Input('boton_entrenar', 'n_clicks'), Input('boton_tratar', 'n_clicks')],
    [State('valor_histo', 'value'),State('valor_busqueda', 'value')],prevent_initial_call = True)
def selector_eliminar(click, pp, entrenar, tratar, marca, criterio):    
    
    suma = 132
    if click == 1:
        suma += 48
    if tratar == 1:
        suma += 22
    if entrenar == 1:
        suma += 22
    
    h = f'{suma}px'

    if click == 1:           
        if criterio is not None and criterio != '': 
            
            return [[dcc.Input(id = 'valor_eliminar',style = {'width': '252px','height': '20px'}, type = 'text', value = f'{marca},{criterio}'), 
                    html.Button(style = {'width': '260px'}, children = 'Aceptar', id = 'aceptar_eliminar', n_clicks = 0)], 
                    click,
                    {'position': 'absolute','left': '1290px', 'top': '0px', 'width': '285px', 'height': h, 'padding-left': '6px', 'background': '#C8D9DE'}]
        
        return [[dcc.Input(id = 'valor_eliminar',style = {'width': '252px','height': '20px'}, type = 'text', value = '', placeholder = 'Columna,Criterio'), 
                 html.Button(style = {'width': '260px'}, children = 'Aceptar', id = 'aceptar_eliminar', n_clicks = 0)], 
                 click,
                {'position': 'absolute','left': '1290px', 'top': '0px', 'width': '285px', 'height': h, 'padding-left': '6px', 'background': '#C8D9DE'}]
    else:
        
        return [[html.Td(id = 'valor_eliminar'), html.Td(id = 'aceptar_eliminar')], 0,
                {'position': 'absolute','left': '1290px', 'top': '0px', 'width': '285px', 'height': h, 'padding-left': '6px', 'background': '#C8D9DE'}] 

    
########################## Tratar csv
@app.callback(
    [Output('selector_tratar', 'children'), Output('boton_tratar', 'n_clicks')],
    [Input('boton_tratar', 'n_clicks')],prevent_initial_call = True)
def selector_histograma(click):    
    if click == 1:         
        return [html.Button(style = {'width': '260px'}, children = 'Aceptar', id = 'aceptar_tratar', n_clicks = 0), click]
    else:
        return [html.Td(id = 'aceptar_tratar'), 0] 

@app.callback(
    Output('entrenar_modelo_div', 'className'),
    [Input('aceptar_tratar', 'n_clicks')],prevent_initial_call = True)
def selector_histograma(click):    
    if click:   
        from Spark.Tratamiento_copy import tratamiento_copy
        try:
            path = 'Datasets/dataset_outlayers.csv'
            tratamiento_copy(path)
        except:
            pass
        return ''

############################ Entrenar Modelo
@app.callback(
    [Output('selector_entrenar', 'children'), Output('boton_entrenar', 'n_clicks')],
    [Input('boton_entrenar', 'n_clicks')],prevent_initial_call = True)
def selector_histograma(click):    
    if click == 1:         
        return [html.Button(style = {'width': '260px'}, children = 'Aceptar', id = 'aceptar_entrenar', n_clicks = 0), click]
    else:
        return [html.Td(id = 'aceptar_entrenarr'), 0] 

@app.callback(
    Output('entrenar_modelo_div', 'style'),
    [Input('aceptar_entrenar', 'n_clicks')],prevent_initial_call = True)
def selector_histograma(click):    
    if click:   
        from Spark.Modelo import modelo   
        try:         
            df = leer_csv('Datasets/dataset_copy_spark.csv').iloc[:,1:]
            modelo(df)  
        except:
            pass
        return {}     


###################################################################################################################################################################   
######################################## Página Historico Consultas ###############################################################################################
###################################################################################################################################################################

@app.callback(
    [Output('tablaHistorico', 'children'),Output('graficoHistorico', 'children'),Output('graficoHistorico2', 'children')],
    [Input('url', 'pathname')])
def tabla_historico(url):    
    df = leer_csv('Prediccion/Resultados/consultas.csv')   
    df2 = df
    
    df2 = df['Fecha'].str.split(' ', expand=True)
    df2['Marca'] = df['Marca']
    df2['Prediccion'] = df['Prediccion']
    
    fig = px.bar(df2, x = df2.iloc[:,0], color='Marca', height = 400)
    fig2 = px.bar(df2, x = df2.iloc[:,0], color='Prediccion', height = 400)

    return [[dataTable('data_table',df,'300px','1450px'),html.Div(id='output_div')],
            html.Div([dcc.Graph(style = {'height': '365px'}, id='life-exp-vs-gdp', figure = fig)]),
            html.Div([dcc.Graph(style = {'height': '365px'}, id='life-exp-vs-gdp', figure = fig2)])] 


###################################################################################################################################################################
######################################## Página Modelo ############################################################################################################
###################################################################################################################################################################


@app.callback(Output('aceptarSpark', 'n_clicks'),
             Input('aceptarSpark', 'n_clicks'),
             [State('selectorSpark', 'value')],prevent_initial_call = True)
def display_page(clickSpark, valueSpark):
    
    if clickSpark > 0:
        if valueSpark == 'Tratar Datos':
            from Spark.Tratamiento import tratamiento
            tratamiento()

        if valueSpark == 'Entrenar Modelo':
            from Spark.Modelo import modelo            
            modelo(leer_csv('Spark/Datasets/df_fusionado.csv').iloc[:,1:])      
            
    return 0

#Descarga del CSV
@app.callback(
    Output('descarga_csv', 'n_clicks'),
    Input('descarga_csv', 'n_clicks'),prevent_initial_call = True)
def descarga_csv(n_clicks):
    if n_clicks > 0:     
        wget.download('https://storage.googleapis.com/web-coches/datos_autocasion.csv')        
        wget.download('https://storage.googleapis.com/web-coches/datos_cochescom.csv')        
        
        try:   
            mueve_fichero('datos_autocasion.csv', 'Spark/Datasets/autocasion_com.csv') 
        except:
            copia_fichero('Spark/Cloud/autocasion_com.csv', 'Spark/Datasets/autocasion_com.csv')
        try:
            mueve_fichero('datos_cochescom.csv', 'Spark/Datasets/datos_coches_com.csv') 
        except:
            copia_fichero('Spark/Cloud/datos_coches_com.csv', 'Spark/Datasets/datos_coches_com.csv') 
        try:
            mueve_fichero('cochesinternet.net.csv', 'Spark/Datasets/cochesinternet.net.csv')               
        except:
            copia_fichero('Spark/Cloud/cochesinternet.net.csv', 'Spark/Datasets/cochesinternet.net.csv')        
        
    return 0
    

###################################################################################################################################################################
######################################## Página Averias ###########################################################################################################
###################################################################################################################################################################



@app.callback(
    Output('tablaAverias', 'children'),
    [Input('marcaAverias', 'value'), Input('kiloAverias', 'value'), Input('anyoAverias', 'value')],
    [State('marcaAverias', 'value'), State('kiloAverias', 'value'), State('anyoAverias', 'value')])
def criterio_busqueda(a,b,c, marca, kilo, anyo): 

    if len(str(marca)) > 0 and len(str(kilo)) > 0 and len(str(anyo)) > 0:
        if marca is not None and kilo is not None and anyo is not None:
            from joblib import load
            import pandas as pd

            averias = ['FRENOS', 'ALTERNADOR', 'CARBURADOR', 'AMORTIGUADOR', 'EMISIONES', 'ESCAPE',
                       'CILINDROS', 'MOTOR DE ARRANQUE', 'DIRECCION', 'EMBRAGUE', 'ABC MOTOR',
                       'SISTEMA ELECTRICO', 'RADIADOR', 'CORREA']
            
            scaler = load('Averias/Modelos/std_scaler.bin')    
            modelo = load('Averias/Modelos/modelo_entrenado.pkl')       
            
            con = pd.DataFrame({'ANIO_VEHICULO': [anyo], 'KILOMETRAJE': [kilo], 'MARCA': [marcas[marca]], 'AVERIA': [0]})            
            con = scaler.transform(con.loc[:, con.columns != 'AVERIA']  )            
            
            consulta = dict()
            for i,x in enumerate(modelo.predict_proba(con)[0]):
                consulta[averias[i]] = [f'{round(x * 100, 2)}%']

            df = pd.DataFrame(consulta)
            
            return [dataTable('data_table',df, alto = '70px', ancho = '1500px'),
                    html.Div(id='output_div')]
    return []



###################################################################################################################################################################
############################################################# Página Dashboard ####################################################################################
###################################################################################################################################################################

@app.callback(
    Output('barplot_ventas_seg', 'figure'),
    [Input('marca4', 'value')],
    [State('orden4', 'value')])
def actualizar_graph_seg(marca, orden):
    
    if marca is not None and len(marca) > 0:
       
        ordenado = True
        if (orden == 'Primero mas alto'): ordenado = False        
        df = leer_csv('Datasets/dataset.csv').sort_values('Precio', ascending = ordenado)
        df['Años'] = df['Anyo']
        df = df[['Precio', 'Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Años', 'Puertas', 'Cambio', 'Color', 'Metalizado']]        
        df = df[df["Marca"]==marca]
        
        df_agrupado = df.groupby("Modelo")["Color"].agg("count").to_frame(name = "TotalModelo").reset_index()   
        
        trace = {
            'data': [go.Bar(x=df_agrupado["TotalModelo"],
                            y=df_agrupado["Modelo"],
                            orientation='h',
                            marker_color='teal', 
                            marker_line_color='rgb(8,48,107)',
                            marker_line_width=1
                        )],
            'layout': go.Layout(
                title= f'Modelos de la marca : {marca}',
                xaxis={'title': " Total Modelos"},
                yaxis={'title': "Modelo"},
                hovermode='closest',
                height=310)                
            }
        return go.Figure(data=trace)   

    trace_vacio = {
        'data': [go.Scatter(x=[],
                        y=[]
                            )],
        'layout': go.Layout(
            title="",
            xaxis={'title': ""},
            yaxis={'title': ""},
            hovermode='closest',
            height=310
        )} 
    return go.Figure(data=trace_vacio)

@app.callback(
    [Output('barplot_beneficio_cat', 'figure'), Output('lineplot_cantidad', 'figure'),
     Output('scatter_precio_anos', 'figure'), Output('tabla4', 'figure')],
    [Input('marca4', 'value'), Input('barplot_ventas_seg', 'hoverData')],
    [State('orden4', 'value')])
def actualizar_graph_cat(marca, hoverData, orden ):

    if hoverData is not None and marca is not None and len(marca) > 0:

        v_index = hoverData['points'][0]['y']    
        ordenado = True
        if (orden == 'Primero mas alto'): ordenado = False        
        df = leer_csv('Datasets/dataset.csv').sort_values('Precio', ascending = ordenado)
        df['Años'] = df['Anyo']
        df = df[['Precio', 'Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Años', 'Puertas', 'Cambio', 'Color', 'Metalizado']]                
        
        ######################################
        df2 = df[(df["Marca"]==marca) & (df["Modelo"]==v_index)]         
        trace_cc = {
        'data': [go.Bar(x=df2.groupby("CC")["Color"].agg("count").to_frame(name = "TotalCC").reset_index()["CC"],
                        y=df2.groupby("CC")["Color"].agg("count").to_frame(name = "TotalCC").reset_index()["TotalCC"],
                        marker_color='LightSeaGreen', 
                        marker_line_color='rgb(8,48,107)',
                        marker_line_width=1.5, 
                        opacity=0.6
                    )],
        'layout': go.Layout(
            title= f'CC para {marca} {v_index}',
            xaxis={'title': "CC"},
            yaxis={'title': " Total CC"},
            hovermode='closest',
            height=310
        )}

        ######################################       
        trace_anyos = {
        'data': [go.Scatter(x=df2.groupby("Años")["Color"].agg("count").to_frame(name = "TotalAnos").reset_index()["Años"],
                            y=df2.groupby("Años")["Color"].agg("count").to_frame(name = "TotalAnos").reset_index()["TotalAnos"],
                            mode="lines+markers"
                            )],
        'layout': go.Layout(
            title=f'Vehículos por año para {marca} {v_index}',
            xaxis={'title': "Años"},
            yaxis={'title': " Total Años"},
            hovermode='closest',
            height=310
        )}

        ######################################        
        df2 = df[(df["Marca"]==marca) & (df["Modelo"]==v_index)]
        trace = {'data': [go.Scatter(
                x = df2["Años"],
                y = df2["Precio"],
                mode='markers',
                marker=dict(
                    size=15,
                    color=df2["Precio"], #set color equal to a variable
                    colorscale='Viridis', # one of plotly colorscales
                    showscale=True
                )
            )],
            'layout': go.Layout(
                title=f'Precio por años para {marca} {v_index}',
                xaxis={'title': "Años"},
                yaxis={'title': "Precio"},
                hovermode='closest',
                height=310
            )}

        ##########################################                         
        df2 = df[df['Marca'] == marca]
        df2 = df2[df2['Modelo'] == v_index]
        trace2 = {'data': [go.Table(
                    header=dict(
                        values=['Precio', 'Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Años', 'Puertas', 'Cambio', 'Color'],
                        font=dict(size=10),
                        align="left"
                    ),
                    cells=dict(
                        values=[df2[k].tolist() for k in df.columns[0:-1]],
                        align = "left"),
                    )],
                'layout': go.Layout(
                    height=600,
                    showlegend=False,
                    title_text=f'Detalle de la Consulta para {marca} {v_index}',
        )}
        
        return [go.Figure(data=trace_cc), go.Figure(data=trace_anyos), go.Figure(data=trace), go.Figure(data=trace2)]

    trace_vacio = {
        'data': [go.Scatter(x=[],
                        y=[]
                            )],
        'layout': go.Layout(
            title="",
            xaxis={'title': ""},
            yaxis={'title': ""},
            hovermode='closest',
            height=310
        )}
    
    return [go.Figure(data=trace_vacio), go.Figure(data=trace_vacio), go.Figure(data=trace_vacio), go.Figure(data=trace_vacio)]








