from dash import dcc, html, dash_table
from csv import writer
from pandas import read_csv
import shutil
import base64


styleTd = {'textAlign': 'center', 'color': 'black'}
styleDrop = {'width': '250px', 'border': '1px solid black'}
styleform = {'margin-top': '8px','margin-left':'10px'}


def header():   
    encoded_image = base64.b64encode(open('assets/logo.png', 'rb').read())    
    return html.Div(children=[html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()))], 
                    style={'padding-left': '20px', 'padding-bottom': '20px'})

def footer():
    encoded_image = base64.b64encode(open('assets/logo.png', 'rb').read())
    return html.Div(children=[html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()))], 
                    style={'position': 'absolute', 'right': '15px', 'bottom':'-5px'})

def selector(title,id,options,value,multi = False,width = 170, pos = '0px', arr = '0px'):
    return html.Div(style = {'position': 'absolute','left': pos,'top': arr, 'width': width},children=[
                html.Td(style = styleTd, children = title),
                dcc.Dropdown(id = id, style = {'width': width, 'border': '1px solid black'},
                            options = options, value = value, multi = multi)])

def selector_outlayer(title,id,options,value,multi = False,width = 170, pos = '0px', arr = '0px'):
    return html.Div(style = {'width': width, 'margin-left': pos, 'margin-top': arr},children=[
                dcc.Dropdown(id = id, style = {'width': width, 'border': '0.2px solid black'},
                             options = options, value = value, multi = multi)])

def dataTable(id, df, alto = '500px', ancho = '1050px', top = '20px'):
    return dash_table.DataTable(id = 'data_table',
                                data = df.to_dict('records'),
                                columns = [{'name': i, 'id': i} for i in df.columns],
                                style_cell = {'textAlign': 'left',                                              
                                              'color': 'black',
                                              'fontSize': 14,
                                              'fontFamily': 'sans-serif'},
                                style_table = {'margin-top': top,
                                               'height': alto,
                                               'width': ancho,
                                               'overflowY': 'scroll',
                                               'overflowX': 'scroll'})
######## Ficheros ##############

def leer_csv(path):
    return read_csv(path)

def guardar_csv(df, path):
    df.to_csv(path, index = False)
    
def copia_fichero(path,path2):
    shutil.copyfile(path,path2) 

def mueve_fichero(path,path2):
    shutil.move(path,path2)

def actualizar_csv(path,list_data):
    with open(path, 'a', newline='') as f_object:     
            writer_object = writer(f_object)
            writer_object.writerow(list_data)

################################

def fondo_pantalla(path, wi = '1600px', he = '735px', iz = '0px', arr = '0px'):
    import base64
    encoded_image = base64.b64encode(open(path, 'rb').read())
    return html.Div(id = 'fondo', style = {'position': 'absolute','left': iz, 'top': arr, 'width': '100%', 'height': '100%'},children = [
                    html.Img(style = {'width': wi, 'height': he}, src='data:image/png;base64,{}'.format(encoded_image.decode()))
           ])