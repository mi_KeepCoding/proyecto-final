from dash import dcc, html
import plotly.express as px

from components.listas import *
from components.componentes import leer_csv


############################################# Pagina 1 ################################################################

styleform = {'margin-top': '5px','margin-left':'10px'}
styleDrop = {'width': '250px', 'border': '1px solid black'}

####### Formulario #########
                        
def dropDowns(id, options, value):
    return html.Div(style = styleform, children=[
                        html.Td(children = id),
                        dcc.Dropdown(id = id.lower(), style = styleDrop,
                                     options = options, value = value, multi = False)])

def formulario(tipo = 0):

    obj = []
    id = ['Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Anyos', 'Puertas', 'Cambio', 'Color', 'Metalizado']
    op = [sorted(marcas), [], cc, potencia, sorted(combustible), kilometros, anyos, [3,5], ['manual', 'automatico'],
          sorted(color), ['Si','No']] 
    vals = ['', [], [], [], [], [], [], [], [], [], []]    
    
    if tipo == 1:
        data = leer_csv('Matricula/data/data.csv')
        op = [sorted(marcas), sorted(modelos[data['Marca'][0]]), cc, potencia, sorted(combustible), kilometros, anyos, [3,5],
              ['manual', 'automatico'], sorted(color), ['Si','No']] 
        vals = [data['Marca'][0], data['Modelo'][0], data['CC'][0], data['Potencia'][0], data['Combustible'][0], '', data['Anyo'][0], data['Puertas'][0], [], [], []]
        
        obj += [html.Div(style = styleform, children=[
                            html.Td(children = 'Matrícula'),
                            dcc.Input(id = 'matricula', type = 'text', value = data['Matricula'][0], style = {'width': '30%'})
                        ])]

    for i in range(11):
        obj.append(dropDowns(id[i],op[i], vals[i]))  

    return obj

############ Caja de páginas ##############

def buscador():
    
    obj = []
    for page in paginas:
        obj.append(html.Div(html.A(paginas[page],href=f'{page}', style={'font-size': '16px','font-weight': 'bold', 'color': 'black'})))
    return obj

######## Plotly #########

def bar(df,x,y,color,title):
    return px.bar(df, x=x, y=y, color=color, title=title)


