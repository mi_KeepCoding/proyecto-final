# -*- coding: utf-8 -*-

# Para ejecutarlo spark-submit Tratamiento.py o python3 Tratamiento.py

from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import udf, expr
from pyspark.sql.types import StringType,IntegerType
import pandas as pd

def tratamiento():
    spark = SparkSession\
        .builder\
        .appName("Pipeline")\
        .getOrCreate()
        
    def string(string):
        if string is not None: 
            return " ".join(string.split(' ')[1:])
        return ""

    udfString = F.udf(string, StringType())

    def cc(string): 
        cil = ['1.0','1.1','1.2','1.3','1.4','1.5','1.6','1.7','1.8','1.9','2.0','3.0','4.0','5.0','6.0']
        for x in cil: 
            if string.find(x) != -1:  
                return x   
                                
    udfCc = F.udf(cc, StringType())       

    def marca(string):   
    
        try:
            return string.split('"')[1]
        except:
            return string
        
    udfMarca = F.udf(marca, StringType()) 

    def modelo(string):
        
        marca = string.split(' ')[0].upper()     
        marca = marca.split('"') 
        try:
            marca = marca[1]
        except:
            marca = marca[0]
        

        
        marcas = {'AUDI': ['a3','a6','q5','a8','a1','a4','a1','q535','q2','a5','a7','a5','q335','q750','q3','e-tron','q745','q8','ttcoupe','r8','q7','tt','allroadquattro',
                        'cabriors'],
                'ABARTH': ['500595','500695','500595','500500','595595'], 
                'ASTON': ['martin'],
                'ALFA': ['romeo'],
                'BENTLEY': ['mulsanne','continental','bentayga'],
                'BMW' : ['i1','i2','i3','i4','i5','i6','i7','i8','x1','x2','x3','x4','x5','x6','x7','x8','z1','z2','z3','z4','z5','z6','z7','z8','serie','ix'],
                'CHEVROLET': ['corvette','epica','kalos','captiva','aveo','camaro','matiz','orlando','cruze','silverado','equinox','lacetti','trailblazer','spark'],
                'CADILLAC': ['bls','escaladeesv','cts'], 
                'CITROEN': ['nemo','c-elysée','space','xsara','clairscape','saxo','berlingo','c-crosser','ds3','ds4','c1','c2','c3','c4','c5','c6','c7','c8','c-zero','xantia','jumpy',
                            'e-mehari','jumper'],
                'CUPRA': ['ateca','león','formentor'],
                'CHRYSLER': ['stratus','neon','voyager','sebring','pt','crossfire'],
                'DS': ['ds3','ds4','ds5','ds6','ds7','ds8','ds9'],
                'DODGE': ['avenger','journey','ram','challenger'],            
                'DAIMLER': ['double'],             
                'DR': ['1','2','3','4','5','6'],  
                'DAEWOO': ['lacetti','matiz','lanos','nubira','evanda'],
                'DFSK': ['f5','glory','seres','serie'],
                'DACIA': ['logan','sandero','duster','lodgy','dokker'],
                'FERRARI': ['ff','612','348','california','laferraricoupé','355','f430','458','488','roma','gtc','599','f8'],
                'FORD': ['puma','focus','c-max','f-150','mondeo','kabitono','edge','eco','mustang','fiesta','fusion','tourneo','kuga','s-max','b-max','transit','galaxy','ka',
                        'explorer','ranger','connect','sierra'],
                'FIAT': ['bravo','tipo','panda','punto','bravo','brava','fiorino','freemont','dobló','qubo','500','600','talento','ducato','124','línea','scudo'],            
                'HYUNDAI': ['bayon','genesis','sonata','terracan','h350','h-1','ioniq','tucson','kona','getz','santa','i1','i2','i3','ix20','i40','ix35','atos'],
                'HONDA': ['jazz','civic','accord','cr-v','hr-v','honda','ridgeline','fr-v'],             
                'HUMMER': ['h1','h2','h3','h4','h5'],
                'INFINITI': ['ex30','q30','q50','ex37','g37','qx30','qx70','q70','q60','fx','g','qx50'], 
                'ISUZU': ['serie','d-max'],
                'INVICTA': ['d2s'],
                'IVECO': ['daily'],
                'JAGUAR': ['i-pace','f-pace','e-pace','f-type','x-type','xe','xf','xj','xk'],
                'JEEP': ['compass','renegade','wrangler','grand','cherokee','gladiator'], 
                'KIA': ['carnival','stonic','shuma','xceed','soul','cee´d','xceed','carens','ceed','sephia','niro','picanto','rio','optima','sorento','sportage','magentis','cerato','stinger','ev6'],
                'KARMA': ['revero'],
                'KTM': ['x-bow'],             
                'LANCIA': ['ypsilon','delta','lybra','thema'], 
                'LOTUS': ['evora'],
                'LAMBORGHINI': ['aventador','huracán','urus'],
                'LEXUS': ['ux','ct','gs','is','rc','nx','rx','ls','lc','es'],            
                'LAND-ROVER': ['defender','discovery','range','freelander','rover'],
                'LAND': ['defender','discovery','range','freelander','rover'],
                'MCLAREN': ['720'],
                'MITSUBISHI': ['lancer','l200','grandis','colt','outlander','space','montero','asx200','asx180','eclipse','asx160','asx170','asx190','asx'],
                'MASERATI': ['quattroporte','ghibli','granturismo','3200gt','levante430','levante','coupé'],
                'MG': ['mgfvvc','marvel','suv','vvc','luxury','mgf','marvel','zs','hs','1.5','1.0','r'],            
                'MERCEDES-BENZ' : ['c200d','b180','glc300','ccoupé','cla220','c200','c220','cla220','glc200','a200','c180','cla200','aa','r350','a200','b200','cla180','a220','e350','gle500',
                                        'a180','ecabrio','am53','shooting','v-benz','e200','eqa250','slk200','cls320','viano','cestate','gle250','r320','r63','rberlina','s300','s320','s350','s350',
                                        's500','s560','s580','scoupé','sl350','sl400','sl500','sl55','sl63','slk250','ss-class','v220','v220','v250','v250','v300','vmonovolumen','vv-class',
                                        'gla(+)','gla250','gla50','glaberlina','glb-benz','glb250','glc250','glc250','glc350','glc350','glc350e','glc63','glc-class','gle-benz','gle300','glecoupé',
                                        'eamg','ecoupé','111','a45','a160','e220','e220','gle-class','glk220','glk320','mercedes-amg','gt53','gtcoupé','gtodoterreno','gtroadster',
                                        'g500','g400','c-benz','cl500','clk200','b250','cls220','eestate','claberlina','a35','a250','e300','cberlina','clsberlina','c300','c43','a250','a35',
                                        'cc-class','cl63','a170','b220','aberlina','efamiliar','csportcoupé','e400','e63','c250','bb-class','113','c250','c63','gl320','a','cla250','cla250','ecoupe',
                                        'g63','clcla-class','a140','epequeño','clase','ccabrio','109','a150','4matic','clk240','cfamiliar','cdeportivo','clkdeportivo','ee-class','cla35','bpequeño',
                                        'clasedan','b220','c250','clk220','gle','glc','ml','mml','c','eqs','amg','eqa','eqb','citan','vito','eqc','sprinter','viano','eqv'],                
                'MAZDA': ['mazda2','mazda3','mazda4','mazda5','mazda6','cx-2','cx-3','cx-4','cx-5','cx-6','cx-7','mx-2','mx-3','mx-4','mx-5','mx-6','mx-7','rx-8'],
                'MINI' : ['countrymancooper','countrymanone','countryman','minijohn','minicabrio','miniclubman','minicooper','minione','minicoupé','john','one','clubmancooper','mini'],
                'NISSAN': ['note','micra','qashqai','370z','leaf','gt-r','pathfinder','x-trail','pulsar','primera','juke','pixo','terrano','murano','patrol','evalia','nv400','350z',
                        'atleon','navara','nt400','nv200','cabstar','nv250','nv300'],
                'OPEL': ['astra','corsa','insignia','mokka','antara','adam','meriva','crossland','zafira','vectra','frontera','grandland','cabrio','agila','combo','vivaro','movano','karl'],
                'PEUGEOT': ['106','107','108','205','206','207','208','305','306','307','308','405','406','407','408','505','506','507','508','605','606','607','608','706','707','708','806','807',
                            '808','1007','1008','1006','2006','2007','2008','3006','3007','3008','4006','4007','4008','5006','5007','5008','6006''6007','6008','partner','rifter','rcz','traveler',
                            'traveller','boxer','bipper','expert'],                         
                'PLYMOUTH': ['prowler'],
                'PORSCHE' : ['panamera','cayenne','taycan','macan','boxster','cayman','911','928','968'],
                'RENAULT' : ['talisman','twingo','mégane','kadjar','clio','modus','captur','laguna','scénic','arkana','espace','kangoo','koleo','fluence','zoe','twizy','maxity','master','trafic'],
                'ROLLS-ROYCE': ['silverseraph'],
                'ROVER': ['200216','75','45'],
                'SUBARU': ['forester','impreza','xv','outback','wrx'],
                'SUZUKI': ['swace','s-cross','baleno','alto','vitara','swift','across','grand','ignis','jimny'],  
                'SKODA': ['yeti','rapid','octavia','fabia','karoq','kodiaq','superb','kamiq','spaceback','scala','citigo','enyaq','roomster'],  
                'SMART': ['forfour','fortwo','smartpequeño','smart'],
                'SAAB': ['9-3','9-5'],
                'SEAT': ['león','altea','exeo','ibiza','ateca','arona','toledo','alhambra','tarraco','mii','ronda','málaga','córdoba'],
                'SUZUKI': ['ignis','vitara','across','grand','vitara','swift','jimny','sx4','swace','s-cross','baleno','alto'],
                'SSANGYONG': ['rodius','kyron','actyon','tivoli','xlvg','korando','rexton','xlv'], 
                'SWM': ['g01f','g01'],
                'TATA': ['indica','aria'],
                'TESLA': ['model'], 
                'TOYOTA': ['rav-4','corolla','yaris','auris','verso','avensis','c-hr','prius','aygo','iq2','land','mirai','urban','gt86','iq','hilux','supra','celica','proace','4-runner'],
                'VOLVO' : ['c30','c40','c50','c60','c70','c80','c90','xc30','xc40','xc50','xc60','xc70','xc80','xc90','s30','s40','s50','s60','s70','s80','s90','v30','v40','v50','v60','v70','v80','v90'],            
                'VOLKSWAGEN' : ['tiguan', 't-roc','id.3','id.4','touran','passat','polo','up!','golf','beetle','t-cross','scirocco','transporter','crafter','amarok','multivan','sportsvan',
                                'fox','jetta','up','touareg','arteo','sharan','caddy','caravelle','taigo','california','eos','cc']  
                }
        try:
            for x in marcas[marca.upper()]:
                if x in string.lower():
                    return(x)
        except:
            return None

    udfModelo = F.udf(modelo, StringType()) 

    puertas = """ case when Puertas in ('targa', 'descapotable', 'convertible', 'deportivo', 'coupe', 'pequeño', '2 puertas', '3 puertas', '3') then 3 
                           else 5 
                           end as Puertas"""
    combustible = """ case when Combustible in ('gasolina', 'gasolina/gas', 'gas') then 0 
                           else case when Combustible in ('diesel', 'diésel') then 1
                                else case when Combustible in ('eléctrico', 'electrico', 'corriente eléctrica', 'corriente electrica') then 2
                                     else 3
                                        end
                                end
                            end as Combustible"""
    

    def pipeline(entrada):
        df = spark.read.csv(entrada,inferSchema=True, header =True)
        
        if entrada == 'Spark/Datasets/cochesinternet.net.csv':
            df = (df.withColumn('Modelo', F.col('Mod'))
                    .drop('Mod')
                    .drop('Version')
                    .drop('Extras')
                    .withColumn('Anyo', 2022 - F.col('Anyo'))
                    .withColumn('Precio', F.col('Precio') * 1000)
                    .withColumn('Kilometros', F.col('Kilometros') * 1000)
                 )
        elif (entrada == 'Spark/Datasets/autocasion_com.csv' or entrada == 'Spark/Datasets/datos_coches_com.csv'):
            df = (df.drop('_c0')  #Eliminamos columna _co
                    .drop('Emisiones') #Eliminamos columna Emisiones         
                    .where(F.col('Coche') != 'None') # Eliminamos filas vacías   
                    .where(F.col('Precio') != 'None') # Eliminamos filas vacías       
                    .withColumn('Anyo', F.when(F.size(F.split(F.col("Anyo"),'/')) > 1, 2022 - F.split(F.col("Anyo"),'/')[1])
                                        .otherwise(2022 - F.split(F.col("Anyo"),'/')[0])             
                            ) #Cambiamos la fecha por los años que tiene el coche
                    .where(F.col('Anyo') >= 0) #Eliminamos filas sin año
                    .withColumn('Precio',     F.concat_ws("", F.split(F.split(F.col('Precio'),    '€')[0], '\.')).cast('int')) #Extraemos precio 
                    .withColumn('Kilometros', F.concat_ws('', F.split(F.split(F.col('Kilometros'),' ')[0], '\.')).cast('int')) #Extraemos kilómetros
                    .withColumn('Marca', F.split(F.col('Coche'),' ')[0]) #Extraemos la marca de la columna coche              
                    .withColumn('Modelo', udfModelo(F.col('Coche'))) #Extraemos el modelo a partir de columna coche
                    .withColumn('Coche', udfString(F.col('Coche'))) #Eliminamos la marca en la columna coche
                    .withColumn('CC', udfCc(F.col('Coche')))
                    .withColumn('Potencia', F.split(F.col('Potencia'), ' ')[0].cast('int'))
                    .drop('Coche') 
        )

        df = (df.withColumn('Puertas', F.lower('Puertas'))
                .withColumn('Puertas', expr(puertas))
                .withColumn('Cambio', F.lower('Cambio'))
                .withColumn('Cambio',F.when(F.col('Cambio').contains('automático') | F.col('Cambio').contains('automatico') , 1)
                                    .otherwise(0))
                .withColumn('Combustible', F.lower('Combustible'))
                .withColumn('Combustible', expr(combustible))
                .withColumn('Color', F.lower('Color'))
                .withColumn('Metalizado', F.when(F.col('Color').contains('metalizado'), 1)
                                        .otherwise(0))                 
                .withColumn('Marca', udfMarca(F.col('Marca')))
                .withColumn('Marca', F.upper('Marca'))
                .na.drop("any").cache()
                .dropDuplicates() 
        )

        ############# DataFrame de para entrenar modelo #############               
            
        df_modelo = (df.withColumn('Marca', F.when(F.col('Marca').contains('ABARTH'), 27).when(F.col('Marca').contains('ALFA'), 30)
                                          .when(F.col('Marca').contains('ASTON'), 7).when(F.col('Marca').contains('AUDI'), 15)
                                          .when(F.col('Marca').contains('BENTLEY'), 16).when(F.col('Marca').contains('BMW'), 39)
                                          .when(F.col('Marca').contains('CADIL'), 48).when(F.col('Marca').contains('CHEV'), 28)
                                          .when(F.col('Marca').contains('CHRYSLER'), 59).when(F.col('Marca').contains('CITROEN'), 14)
                                          .when(F.col('Marca').contains('CUPRA'), 22).when(F.col('Marca').contains('DACIA'), 6) 
                                          .when(F.col('Marca').contains('DAEWO'), 38).when(F.col('Marca').contains('DAIMLER'), 43)
                                          .when(F.col('Marca') == 'DFSK', 58).when(F.col('Marca').contains('DODGE'), 26)
                                          .when(F.col('Marca') == 'DS', 50).when(F.col('Marca').contains('DR'), 4)
                                          .when(F.col('Marca').contains('FERRARI'), 21).when(F.col('Marca').contains('FIAT'), 8)
                                          .when(F.col('Marca').contains('FORD'), 19).when(F.col('Marca').contains('HONDA'), 46)
                                          .when(F.col('Marca').contains('HUMMER'), 45).when(F.col('Marca').contains('HYUNDAI'), 5)
                                          .when(F.col('Marca').contains('INFINITI'), 18).when(F.col('Marca').contains('JAGUAR'), 29)
                                          .when(F.col('Marca').contains('JEEP'), 33).when(F.col('Marca').contains('KARMA'), 9)
                                          .when(F.col('Marca') == 'KIA', 44).when(F.col('Marca') == 'KTM', 57)
                                          .when(F.col('Marca').contains('LAMBORGHINI'), 2).when(F.col('Marca').contains('LANCIA'), 55)
                                          .when(F.col('Marca').contains('LAND-ROVER'), 61).when(F.col('Marca').contains('LEXUS'), 32)
                                          .when(F.col('Marca').contains('LOTUS'), 53).when(F.col('Marca').contains('MASERA'), 40)
                                          .when(F.col('Marca').contains('MAZDA'), 37).when(F.col('Marca').contains('MERCEDES'), 56)
                                          .when(F.col('Marca') == 'MG', 41).when(F.col('Marca').contains('MINI'), 23)
                                          .when(F.col('Marca').contains('MITSU'), 60).when(F.col('Marca').contains('MCLAREN'), 35)
                                          .when(F.col('Marca').contains('NISSAN'), 13).when(F.col('Marca').contains('OPEL'), 54)
                                          .when(F.col('Marca').contains('PEUG'), 25).when(F.col('Marca').contains('PLYM'), 52)
                                          .when(F.col('Marca').contains('PORSCHE'), 3).when(F.col('Marca').contains('RENA'), 49)
                                          .when(F.col('Marca').contains('ROLLS'), 51).when(F.col('Marca').contains('ROVER'), 1)
                                          .when(F.col('Marca').contains('SAAB'), 24).when(F.col('Marca').contains('SEAT'), 20)
                                          .when(F.col('Marca').contains('SKODA'), 11).when(F.col('Marca').contains('SMART'), 47) 
                                          .when(F.col('Marca').contains('SANGYONG'), 31).when(F.col('Marca').contains('SUBARU'), 12)                                                                              
                                          .when(F.col('Marca').contains('SUZUKI'), 17).when(F.col('Marca').contains('TESLA'), 36)
                                          .when(F.col('Marca').contains('TOYOTA'), 10).when(F.col('Marca').contains('VOLKS'), 42)
                                          .when(F.col('Marca').contains('VOLVO'), 34)
                                          .otherwise(F.col('Marca')))
                    .withColumn('Marca',F.col('Marca').cast('int'))
                    .withColumn('Color', F.when(F.col('Color').contains('azul') | F.col('Color').contains('blu') |
                                                F.col('Color').contains('blu') | F.col('Color').contains('azúl') |
                                                F.col('Color').contains('cielo') | F.col('Color').contains('mediterranblau') |    
                                                F.col('Color').contains('celeste') | F.col('Color').contains('blau'), 1)                       
                                            .when(F.col('Color').contains('blanc') | F.col('Color').contains('white') |
                                                F.col('Color').contains('iridio') | F.col('Color').contains('bianco') |
                                                F.col('Color').contains('nacarado') | F.col('Color').contains('rodio'), 2)
                                            .when(F.col('Color').contains('gris') | F.col('Color').contains('grau') |
                                                F.col('Color').contains('grey') | F.col('Color').contains('gray') |
                                                F.col('Color').contains('steel') | F.col('Color').contains('daytona') |
                                                F.col('Color').contains('antracita') | F.col('Color').contains('titaniun') |
                                                F.col('Color').contains('metallic') | F.col('Color').contains('plomo') |
                                                F.col('Color').contains('pearl') | F.col('Color').contains('perla'), 3)
                                            .when(F.col('Color').contains('noche') | F.col('Color').contains('negro') |
                                                F.col('Color').contains('dark') | F.col('Color').contains('black') |
                                                F.col('Color').contains('negra') | F.col('Color').contains('oscuro') |
                                                F.col('Color').contains('nero'), 4)
                                            .when(F.col('Color').contains('rojo') | F.col('Color').contains('red') |
                                                F.col('Color').contains('rosso') | F.col('Color').contains('fuego'), 5)
                                            .when(F.col('Color').contains('amarillo') | F.col('Color').contains('yellow'), 6)
                                            .when(F.col('Color').contains('verde') | F.col('Color').contains('green') |
                                                F.col('Color').contains('agata'), 7)
                                            .when(F.col('Color').contains('marron') | F.col('Color').contains('marrón') |
                                                F.col('Color').contains('brown'), 8)
                                            .when(F.col('Color').contains('naranja') | F.col('Color').contains('manhattan') |
                                                F.col('Color').contains('najranja') | F.col('Color').contains('orange'), 9)
                                            .when(F.col('Color').contains('granate'), 10)
                                            .when(F.col('Color').contains('plata') | F.col('Color').contains('silver') |
                                                F.col('Color').contains('plateado'), 11)
                                            .when(F.col('Color').contains('dorado') | F.col('Color').contains('oro') |
                                                F.col('Color').contains('champagne'), 12)
                                            .when(F.col('Color').contains('bronce'), 13)
                                            .when(F.col('Color').contains('platinum') | F.col('Color').contains('platino') |
                                                F.col('Color').contains('platinium'), 14)
                                            .when(F.col('Color').contains('violeta'), 15)
                                            .when(F.col('Color').contains('rosa') | F.col('Color').contains('rose'), 16)
                                            .when(F.col('Color').contains('beige'), 17)
                                            .otherwise(18)
                                            )   
                            .na.drop("any")                                
        )        
        
        ############## DataFrame para hacer exploraciones en la Aplicación ##############
        df_aplicacion = (df.withColumn('Color', F.when(F.col('Color').contains('azul') | F.col('Color').contains('blu') |
                                                    F.col('Color').contains('blu') | F.col('Color').contains('azúl') |
                                                    F.col('Color').contains('cielo') | F.col('Color').contains('mediterranblau') |    
                                                    F.col('Color').contains('celeste') | F.col('Color').contains('blau'), 'Azul')                       
                                                .when(F.col('Color').contains('blanc') | F.col('Color').contains('white') |
                                                    F.col('Color').contains('iridio') | F.col('Color').contains('bianco') |
                                                    F.col('Color').contains('nacarado') | F.col('Color').contains('rodio'), 'Blanco')
                                                .when(F.col('Color').contains('gris') | F.col('Color').contains('grau') |
                                                    F.col('Color').contains('grey') | F.col('Color').contains('gray') |
                                                    F.col('Color').contains('steel') | F.col('Color').contains('daytona') |
                                                    F.col('Color').contains('antracita') | F.col('Color').contains('titaniun') |
                                                    F.col('Color').contains('metallic') | F.col('Color').contains('plomo') |
                                                    F.col('Color').contains('pearl') | F.col('Color').contains('perla'), 'Gris')
                                                .when(F.col('Color').contains('noche') | F.col('Color').contains('negro') |
                                                    F.col('Color').contains('dark') | F.col('Color').contains('black') |
                                                    F.col('Color').contains('negra') | F.col('Color').contains('oscuro') |
                                                    F.col('Color').contains('nero'), 'Negro')
                                                .when(F.col('Color').contains('rojo') | F.col('Color').contains('red') |
                                                    F.col('Color').contains('rosso') | F.col('Color').contains('fuego'), 'Rojo')
                                                .when(F.col('Color').contains('amarillo') | F.col('Color').contains('yellow'), 'Amarillo')
                                                .when(F.col('Color').contains('verde') | F.col('Color').contains('green') |
                                                    F.col('Color').contains('agata'), 'Verde')
                                                .when(F.col('Color').contains('marron') | F.col('Color').contains('marrón') |
                                                    F.col('Color').contains('brown'), 'Marrón')
                                                .when(F.col('Color').contains('naranja') | F.col('Color').contains('manhattan') |
                                                    F.col('Color').contains('najranja') | F.col('Color').contains('orange'), 'Naranja')
                                                .when(F.col('Color').contains('granate'), 'Granate')
                                                .when(F.col('Color').contains('plata') | F.col('Color').contains('silver') |
                                                    F.col('Color').contains('plateado'), 'Plata')
                                                .when(F.col('Color').contains('dorado') | F.col('Color').contains('oro') |
                                                    F.col('Color').contains('champagne'), 'Dorado')
                                                .when(F.col('Color').contains('bronce'), 'Bronce')
                                                .when(F.col('Color').contains('platinum') | F.col('Color').contains('platino') |
                                                    F.col('Color').contains('platinium'), 'Platinum')
                                                .when(F.col('Color').contains('violeta'), 'Violeta')
                                                .when(F.col('Color').contains('rosa') | F.col('Color').contains('rose'), 'Rosa')
                                                .when(F.col('Color').contains('beige'), 'Beige')
                                                .otherwise('Otro')                                    
                                                )
                            .na.drop("any")                 
            )

        df_aplicacion = df_aplicacion['Precio','Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros',	'Anyo',	'Puertas', 'Cambio', 'Color', 'Metalizado']
        df_modelo = df_modelo['Precio','Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros',	'Anyo',	'Puertas', 'Cambio', 'Color', 'Metalizado']
        df.unpersist()
        
        return df_aplicacion, df_modelo        

    df_1, df2_1, = pipeline('Spark/Datasets/autocasion_com.csv')
    df_2, df2_2, = pipeline('Spark/Datasets/datos_coches_com.csv')
    df_3, df2_3, = pipeline('Spark/Datasets/cochesinternet.net.csv')

    df1 = df_1.union(df_2.select(df_1.columns))
    df1 = df1.union(df_3.select(df1.columns))
    df1.toPandas().to_csv('Datasets/dataset.csv')
    df1.toPandas().to_csv('Datasets/dataset_outlayers.csv') 

    df2 = df2_1.union(df2_2.select(df2_1.columns))
    df2 = df2.union(df2_3.select(df2.columns))
    df2.toPandas().to_csv('Spark/Datasets/df_fusionado.csv')








