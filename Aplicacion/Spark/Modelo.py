# -*- coding: utf-8 -*-

# Para ejecutarlo python3 Tratamiento.py

import pandas as pd

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor

from joblib import dump

def modelo(df):    
    
    mean_map = {}
    for c in ['Modelo']:
        mean = df.groupby(c)['Precio'].mean()
        df[c] = df[c].map(mean)    
        mean_map[c] = mean
 
    dump(mean_map, 'Prediccion/Modelos/mean_map.pkl') 

    train, test = train_test_split(df, test_size=0.2, shuffle=True, random_state=0)
    print('Dimensiones del dataset de training:', train.shape)
    print('Dimensiones del dataset de test:', test.shape)
    
    y_train = train['Precio'] 
    X_train = train.loc[:, test.columns != 'Precio']    
    
    y_test = test['Precio'] 
    X_test = test.loc[:, test.columns != 'Precio']    
    
    # Escalamos (con los datos de train)
    scaler = preprocessing.StandardScaler().fit(X_train)
    XtrainScaled = scaler.transform(X_train)
    XtestScaled = scaler.transform(X_test)  
    dump(scaler, 'Prediccion/Modelos/std_scaler.bin', compress=True)        
        
    maxDepthOptimo = 12
    modelo = RandomForestRegressor(max_depth=maxDepthOptimo).fit(XtrainScaled, y_train)
    print("Accuracy en train:", modelo.score(XtrainScaled, y_train))
    print("Accuracy en test:", modelo.score(XtestScaled, y_test))  
    dump(modelo, 'Prediccion/Modelos/modelo_entrenado.pkl') # Guardo el modelo.
    