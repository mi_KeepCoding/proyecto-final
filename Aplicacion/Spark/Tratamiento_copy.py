# -*- coding: utf-8 -*-

# Para ejecutarlo spark-submit Tratamiento.py o python3 Tratamiento.py

from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType,IntegerType
import pandas as pd

def tratamiento_copy(path):
    spark = SparkSession\
        .builder\
        .appName("Pipeline")\
        .getOrCreate()     

    def pipeline(entrada):
        df = spark.read.csv(entrada,inferSchema=True, header =True)['Precio','Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros',	'Anyo',	'Puertas', 'Cambio', 'Color', 'Metalizado']
        
        ############# DataFrame de para entrenar modelo #############     
        df_modelo = (df.withColumn('Marca', F.when(F.col('Marca').contains('ABARTH'), 27).when(F.col('Marca').contains('ALFA'), 30)
                                          .when(F.col('Marca').contains('ASTON'), 7).when(F.col('Marca').contains('AUDI'), 15)
                                          .when(F.col('Marca').contains('BENTLEY'), 16).when(F.col('Marca').contains('BMW'), 39)
                                          .when(F.col('Marca').contains('CADIL'), 48).when(F.col('Marca').contains('CHEV'), 28)
                                          .when(F.col('Marca').contains('CHRYSLER'), 59).when(F.col('Marca').contains('CITROEN'), 14)
                                          .when(F.col('Marca').contains('CUPRA'), 22).when(F.col('Marca').contains('DACIA'), 6) 
                                          .when(F.col('Marca').contains('DAEWO'), 38).when(F.col('Marca').contains('DAIMLER'), 43)
                                          .when(F.col('Marca') == 'DFSK', 58).when(F.col('Marca').contains('DODGE'), 26)
                                          .when(F.col('Marca') == 'DS', 50).when(F.col('Marca').contains('DR'), 4)
                                          .when(F.col('Marca').contains('FERRARI'), 21).when(F.col('Marca').contains('FIAT'), 8)
                                          .when(F.col('Marca').contains('FORD'), 19).when(F.col('Marca').contains('HONDA'), 46)
                                          .when(F.col('Marca').contains('HUMMER'), 45).when(F.col('Marca').contains('HYUNDAI'), 5)
                                          .when(F.col('Marca').contains('INFINITI'), 18).when(F.col('Marca').contains('JAGUAR'), 29)
                                          .when(F.col('Marca').contains('JEEP'), 33).when(F.col('Marca').contains('KARMA'), 9)
                                          .when(F.col('Marca') == 'KIA', 44).when(F.col('Marca') == 'KTM', 57)
                                          .when(F.col('Marca').contains('LAMBORGHINI'), 2).when(F.col('Marca').contains('LANCIA'), 55)
                                          .when(F.col('Marca').contains('LAND-ROVER'), 61).when(F.col('Marca').contains('LEXUS'), 32)
                                          .when(F.col('Marca').contains('LOTUS'), 53).when(F.col('Marca').contains('MASERA'), 40)
                                          .when(F.col('Marca').contains('MAZDA'), 37).when(F.col('Marca').contains('MERCEDES'), 56)
                                          .when(F.col('Marca') == 'MG', 41).when(F.col('Marca').contains('MINI'), 23)
                                          .when(F.col('Marca').contains('MITSU'), 60).when(F.col('Marca').contains('MCLAREN'), 35)
                                          .when(F.col('Marca').contains('NISSAN'), 13).when(F.col('Marca').contains('OPEL'), 54)
                                          .when(F.col('Marca').contains('PEUG'), 25).when(F.col('Marca').contains('PLYM'), 52)
                                          .when(F.col('Marca').contains('PORSCHE'), 3).when(F.col('Marca').contains('RENA'), 49)
                                          .when(F.col('Marca').contains('ROLLS'), 51).when(F.col('Marca').contains('ROVER'), 1)
                                          .when(F.col('Marca').contains('SAAB'), 24).when(F.col('Marca').contains('SEAT'), 20)
                                          .when(F.col('Marca').contains('SKODA'), 11).when(F.col('Marca').contains('SMART'), 47) 
                                          .when(F.col('Marca').contains('SANGYONG'), 31).when(F.col('Marca').contains('SUBARU'), 12)                                                                              
                                          .when(F.col('Marca').contains('SUZUKI'), 17).when(F.col('Marca').contains('TESLA'), 36)
                                          .when(F.col('Marca').contains('TOYOTA'), 10).when(F.col('Marca').contains('VOLKS'), 42)
                                          .when(F.col('Marca').contains('VOLVO'), 34)
                                          .otherwise(F.col('Marca')))
                    .withColumn('Marca',F.col('Marca').cast('int')) 
                    .withColumn('Color', F.lower('Color'))      
                    .withColumn('Color', F.when(F.col('Color').contains('azul'),1)                       
                                          .when(F.col('Color').contains('blanco'), 2)
                                          .when(F.col('Color').contains('gris'), 3)
                                          .when(F.col('Color').contains('negro'), 4)
                                          .when(F.col('Color').contains('rojo'), 5)
                                          .when(F.col('Color').contains('amarillo'), 6)
                                          .when(F.col('Color').contains('verde'), 7)
                                          .when(F.col('Color').contains('marron'), 8)
                                          .when(F.col('Color').contains('naranja'), 9)
                                          .when(F.col('Color').contains('granate'), 10)
                                          .when(F.col('Color').contains('plata'), 11)
                                          .when(F.col('Color').contains('dorado'), 12)
                                          .when(F.col('Color').contains('bronce'), 13)
                                          .when(F.col('Color').contains('platinum'), 14)
                                          .when(F.col('Color').contains('violeta'), 15)
                                          .when(F.col('Color').contains('rosa'), 16)
                                          .when(F.col('Color').contains('beige'), 17)
                                          .otherwise(18))       
                    .na.drop("any")     
        )   
        
        return df_modelo
        

    df = pipeline(path)
    df.toPandas().to_csv('Datasets/dataset_copy_spark.csv')