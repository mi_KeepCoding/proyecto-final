from dash import dcc, html
from components.componentes import fondo_pantalla

layout = html.Div(children=[         
                    fondo_pantalla('assets/fondo2.jpg'),
                    html.Div(id = 'layout', style = {'position': 'relative','left': '20px', 'top': '10px', 'width': '1288px'}, children=[                            
                        #Histograma
                        html.Div(style = {'height': '35x', 'background': '#C8D9DE'}, children =[     
                            #boton           
                            html.Button(children = 'Gráficos', id = 'boton_histo', n_clicks = 0, 
                                        style = {'margin': '4px','border': '1px solid black', 'width': '1278px', 'font-size': '30px',  
                                                'color': 'white', 'background': '#068EB6'}),                
                            #tabla
                            html.Div(id = 'tabla_histo_div', children =[
                                html.Div(style = {'width': '1280px'}, id = 'tabla_histo', children =[])
                            ]), 
                            #selectores
                            html.Div(id = 'selector_histo', children =[
                                html.Td(id = 'valor_histo')                              
                            ]), 
                            html.Div(id = 'selector_grafico', children =[
                                html.Td(id = 'valor_grafico')                                
                            ]) 
                        ]), 
                        #Busqueda   valor_busqueda
                        html.Div(style = {'position': 'absolute', 'right': '-20px', 'top': '0px'}, id = 'busqueda_div', children =[
                            dcc.Input(style = {'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '0px', 'height': '0px','border': '1px solid white'}, 
                                    id = 'valor_busqueda')
                        ]),  
                        
                        #Layout 2
                        html.Div(id = 'layout2', style = {'position': 'absolute','left': '1290px', 'top': '0px', 'width': '285px', 
                                                        'height': '132px', 'padding-left': '6px', 'background': '#C8D9DE'}, children=[ 
                            #Eliminar outlayers
                            html.Div(children =[                          
                                #boton           
                                html.Button(children = 'Eliminar Outlayers', id = 'boton_eliminar', n_clicks = 0, 
                                            style = {'margin-top': '4px', 'width': '260px', 'border': '1px solid black', 'font-size': '30px',  
                                                    'color': 'white', 'background': '#068EB6'}),                            
                                #selector
                                html.Div(id = 'selector_eliminar', children =[
                                    html.Td(id = 'valor_eliminar'),   
                                    html.Td(id = 'aceptar_eliminar'),                         
                                ]) 
                            ]),
                            #Tratar Csv
                            html.Div(style = {'margin-top': '5px', 'margin-bottom': '5px'}, children =[                          
                                #boton           
                                html.Button(children = 'Tratar Csv', id = 'boton_tratar', n_clicks = 0, 
                                            style = {'width': '260px', 'border': '1px solid black', 'font-size': '30px',  'color': 'white', 'background': '#068EB6'}),                            
                                #selector
                                html.Div(id = 'selector_tratar', children =[
                                    html.Td(id = 'aceptar_tratar')                        
                                ]) 
                            ]),
                            #Entrenar Modelo
                            html.Div(id = 'entrenar_modelo_div', children =[                          
                                #boton           
                                html.Button(children = 'Entrenar Modelo', id = 'boton_entrenar', n_clicks = 0, 
                                            style = {'width': '260px', 'border': '1px solid black', 'font-size': '30px',  'color': 'white', 'background': '#068EB6'}),                            
                                #selector
                                html.Div(id = 'selector_entrenar', children =[
                                    html.Td(id = 'aceptar_entrenar')                        
                                ]) 
                            ]),
                        ])
                    ])    
                ])



    