from dash import dcc, html

from components.componentes import selector, fondo_pantalla

layout = html.Div(children=[         
                    fondo_pantalla('assets/fondo3.jpg'),                
                    html.Div(style = {'position': 'absolute','left': '20px', 'top': '10px', 'width': '1500px', 'height': '650px'}, children =[
                        #Selectores
                        html.Div(style = {'width': '350px', 'height': '53px'}, children =[                                          
                            selector('','selectorSpark',['Tratar Datos','Entrenar Modelo'],[], pos = '0px'),    
                            html.Button(children = 'Aceptar', id = 'aceptarSpark', n_clicks = 0, 
                                    style = {'margin-top': '40px', 'width': '100px', 'border': '1px solid black'})                                        
                        ])
                    ]), 
                    #Descargas
                    html.Div(style = {'position': 'absolute','left': '195px', 'top': '10px', 'width': '125px', 'background': '#C8D9DE'}, children =[
                        html.Div(style = {'width': '193px'}, children =[                        
                            html.Button(style = {'margin-left': '1px'}, children = 'Descargar Scrapy', id = 'descarga_csv', n_clicks = 0)
                        ])
                    ])                
                ])
        


