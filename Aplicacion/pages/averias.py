from dash import dcc, html
from components.componentes import selector, fondo_pantalla
from components.listas import marcas
from Averias.modelo import modelo
from Averias.tratamiento import tratamiento

layout = html.Div(children=[         
                    fondo_pantalla('assets/fondo.jpg'),                
                    html.Div(style = {'position': 'absolute','left': '20px', 'top': '10px', 'background': '#068EB6'}, children =[
                        #Selectores
                        html.Div(style = {'width': '530px', 'height': '57px'}, children =[                                          
                            selector('Marca','marcaAverias',sorted(marcas),'', pos = '5px'),    
                            selector('Kilómetros','kiloAverias',[x for x in range(0,300001,25000)],'', pos = '180px'),
                            selector('Años','anyoAverias',[x for x in range (0,26,1)],'', pos = '355px'),                                                                 
                        ])
                    ]), 
                    #Tabla
                    html.Div(id = 'tablaAverias', style = {'position': 'absolute','left': '25px', 'top': '85px'}, children =['hola']),                          
                ])




