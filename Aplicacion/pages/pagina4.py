from dash import dcc, html
from components.listas import marcas
from components.componentes import selector, fondo_pantalla


layout = html.Div([html.Div( children=[    
            fondo_pantalla('assets/fondo.jpg'),            
            #Selectores y tabla               
            html.Div(style = {'position': 'absolute','left': '20px', 'top': '10px', 'width': '1500px', 'height': '850px'}, children =[
                #Selectores
                html.Div(style = {'width': '500px', 'height': '53px'}, children =[            
                    selector('','marca4',sorted(list(marcas.keys())),[]),
                    selector('','orden4',['Primero mas alto','Primero mas bajo'],'Primero mas bajo', pos ='185px')                    
                ])
            ]),
            #Tablas
            html.Div(style = {'position': 'absolute','left': '0px', 'top': '70px'}, children =[             
                html.Div([dcc.Graph(id='barplot_ventas_seg')],
                                    style={'position': 'absolute','left': '20px', 'top': '0px', 'width': '430px', 'height': '50px'}),
                html.Div([dcc.Graph(id='barplot_beneficio_cat')],
                                    style={'position': 'absolute','left': '480px', 'top': '0px', 'width': '430px', 'height': '100px'}),
                html.Div([dcc.Graph(id='tabla4')],
                                    style={'position': 'absolute','left': '940px', 'top': '0px', 'width': '630px', 'height': '100px'}),
                html.Div([dcc.Graph(id='lineplot_cantidad')],
                                    style={'position': 'absolute','left': '20px', 'top': '335px', 'width': '430px', 'height': '90px'}),   
                html.Div([dcc.Graph(id='scatter_precio_anos')],                
                                    style={'position': 'absolute','left': '480px', 'top': '335px', 'width': '430px', 'height': '90px'})       
            ])  
        ])
    ])
