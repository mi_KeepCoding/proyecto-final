from dash import dcc, html
from components.listas import marcas, potencia, anyos, kilometros
from components.componentes import selector, fondo_pantalla
from components.mform import formulario

layout = html.Div(children=[         
                    fondo_pantalla('assets/fondo.jpg'),                        
                    #Formulario  
                    html.Div(id = 'formulario1',style = {'position': 'absolute','left': '10px', 'top': '10px','width': '270px', 'height': '680px','background': '#068EB6'}, children = formulario()),    
                    html.Div(id = 'formulario2',style = {}, children =[]),  
                    #Boton Vehiculo Entrante e Introducir Datos
                    html.Div(style = {'position': 'absolute','left': '290px', 'top': '5px', 'width': '94px', 'height': '38px', 'background': '#068EB6'}, children=[                        
                        html.Button(style = {'margin-left': '2px', 'margin-top': '1px', 'width': '90px', 'border': '1px solid black'},
                                    children = 'Vehiculo Entrante', id = 'actualizar', n_clicks = 0)]),
                    #Selectores de tabla
                    html.Div(style = {'position': 'absolute','left': '480px', 'top': '2px', 'width': '900px', 'height': '59px', 'background': '#068EB6'}, children =[                     
                        #Selectores
                        selector('Marca','precio_marca',sorted(list(marcas.keys())),[], pos = '8px'),
                        selector('Años','precio_anyo',anyos,[], pos = '186px'),
                        selector('Potencia','precio_potencia',potencia,[], pos = '362px'),
                        selector('Kilometros','precio_kilometros',kilometros,[], pos = '538px'),
                        selector('Precio','orden',['Primero mas alto','Primero mas bajo'],'Primero mas bajo', pos = '723px') 
                    ]),
                    #Tabla
                    html.Div(id = 'tabla', style = {'position': 'absolute','left': '415px', 'top': '85px'}, children =[]),
                    #Selector de fotos
                    html.Div(style = {'position': 'absolute','left': '880px', 'top': '624px', 'width': '131px', 'height': '59px', 'background': '#068EB6'},
                        children =[                     
                            selector('Fotos','foto',['coche.jpg','coche1.jpg','coche2.jpg','coche3.jpg','coche4.jpg'],'coche.jpg', width = 125, pos = '2px'),
                            #Boton Foto
                            html.Div(style = {'margin-top': '60px', 'width': '129px', 'background': '#068EB6'}, children =[
                                html.Button(children = 'Activar', id = 'fotos', n_clicks = 0, style = {'margin': '2px', 'width': '125px', 'border': '1px solid black'})
                            ])
                    ],id = 'div_foto'), 
                    #Boton limpiar
                    html.Div(id = 'div_limpiar', style ={'position': 'absolute','left': '790px', 'top': '70px', 'width': '104px', 'background': '#068EB6'}, children =[
                        html.Td(id = 'limpiar', n_clicks = 0)                   
                    ]),
                    #Prediccion   
                    html.Div(id = 'div_prediccion', style = {'position': 'absolute','left': '291px', 'top': '624px', 'width': '115px', 'height': '64px', 'background': '#068EB6'},
                        children =[
                            html.Div(style = {'margin': '1px'}, children =[
                                dcc.Input(style = {'textAlign': 'center','border': '1px solid black', 'width': '106px','height': '38px'}, 
                                        id = 'prediccion', type = 'text', value = 'Estimación'),
                                html.Button(style = {'margin': '2px', 'textAlign': 'center','border': '1px solid black', 'width': '108px'}, 
                                            id = 'calcular', n_clicks = 0, children = 'Calcular')
                            ])
                    ])           
               ]) 