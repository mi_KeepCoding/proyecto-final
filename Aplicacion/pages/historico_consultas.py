from dash import dcc, html
from components.componentes import fondo_pantalla

layout = html.Div(children=[         
                    fondo_pantalla('assets/fondo3.jpg'),            
                    html.Div( children=[ 
                        html.Div(style = {'position': 'absolute','left': '80px', 'top': '-10px'}, children =[                    
                            #Tabla
                            html.Div(id = 'tablaHistorico', children =[])
                        ])
                    ]),
                    html.Div( children=[ 
                            html.Div(style = {'position': 'absolute','left': '80px', 'top': '322px', 'width': '719px'}, children =[                    
                                #Tabla
                                html.Div(id = 'graficoHistorico', children =[])
                            ])
                        ]),
                    html.Div( children=[ 
                            html.Div(style = {'position': 'absolute','left': '810px', 'top': '322px', 'width': '717px'}, children =[                    
                                #Tabla
                                html.Div(style = {'height': '200px'},id = 'graficoHistorico2', children =[])
                            ])
                    ])
                ])