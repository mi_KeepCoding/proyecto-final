from pandas import DataFrame
from joblib import load
from components.listas import marcas, combustible, color

def prediccion(lista):
    
    scaler = load('Prediccion/Modelos/std_scaler.bin')    
    mod = load('Prediccion/Modelos/modelo_entrenado.pkl')
    modelos = load('Prediccion/Modelos/mean_map.pkl')       

    df = DataFrame(columns = ['Marca', 'Modelo', 'CC', 'Potencia', 'Combustible', 'Kilometros', 'Anyo', 'Puertas', 'Cambio', 'Color', 'Metalizado'])
    df.loc[0] = lista  
    
    df['Cambio'] = 0
    if df['Cambio'][0] == 'automatico': df['Cambio'] = 1
    
    df['Metalizado'] = 0
    if df['Metalizado'][0] == 'Si': df['Metalizado'] = 1       

    df['Combustible'][0] = combustible[df['Combustible'][0]]                   
    df['Color'][0] = color[df['Color'][0]]    
    df['Marca'][0] = marcas[df['Marca'][0]]

    if df['Modelo'][0] == 'leon':
        df['Modelo'] = 'león'
    
    for x,y in modelos.items():
        for marca, valor in y.items():
            if marca == df['Modelo'][0]:
                df['Modelo'] = valor               
                break
        break       

    return round(mod.predict(X = scaler.transform(df))[0],2)





